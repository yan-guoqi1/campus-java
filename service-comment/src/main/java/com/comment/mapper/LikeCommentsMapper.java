package com.comment.mapper;

import DO.comments.LikeCommentsDO;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface LikeCommentsMapper {

    // 判断用户是否点赞
    @Select("select is_like from tb_like_comments where user_id = #{userId} and comment_id = #{commentId}")
    Integer getIsLike(Integer userId, Integer commentId);


    @Insert("insert into tb_like_comments (comment_id, user_id, is_like, created_time) " +
            "VALUES" +
            " (#{commentId},#{userId},#{isLike},#{createdTime})")
    void save(LikeCommentsDO likeCommentsDO);

    // 取消点赞
    @Delete("delete from tb_like_comments where user_id = #{userId} and comment_id = #{commentId}")
    void delete(Integer userId, Integer commentId);

    // 用户由踩变为点赞
    @Update("update tb_like_comments set is_like = 1 where user_id = #{userId} and comment_id = #{commentId}")
    void updata(Integer userId, Integer commentId);

    // 用户有点赞变为踩
    @Update("update tb_like_comments set is_like = 0 where user_id = #{userId} and comment_id = #{commentId}")
    void updateNotLike(@Param("userId") Integer userId, @Param("commentId") Integer commentId);

    @Delete("delete from tb_like_comments where comment_id = #{id}")
    void deleteByCommentId(Integer id);
}
