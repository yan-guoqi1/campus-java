package com.comment.mapper;

import DO.comments.CommentsDO;
import DTO.RootPageCommentDTO;
import DTO.TwoPageCommentDTO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CommentsMapper {
    @Insert("insert into tb_comments " +
            "(chat_id, chat_user_id, user_id, content, created_time, is_first, root_comments_id, to_comment_id, to_comment_user_id) VALUES " +
            "(#{chatId},#{chatUserId},#{userId},#{content},#{createdTime},#{isFirst},#{rootCommentsId},#{toCommentId},#{toCommentUserId})")
    void save(CommentsDO commentsDO);


    // 获取一级评论id
    List<Integer> getRootIdByChat(RootPageCommentDTO rootPageCommentDTO);

    // 获取一级评论具体消息
    @Select("select * from tb_comments where id = #{commentId}")
    CommentsDO getById(Integer commentId);

    // 获取一级评论的二级评论的数量
    @Select("select COUNT(*) from tb_comments where root_comments_id = #{commentId}")
    Integer getTwoNumber(Integer commentId);

    // 获取二级评论数组
    @Select("select id from tb_comments where root_comments_id = #{rootCommentId} and " +
            "(#{lastCommentId} = 0 OR id > #{lastCommentId}) " +
            "limit #{number}")
    List<Integer> getTwoIds(TwoPageCommentDTO twoPageCommentDTO);

    @Delete("delete from tb_comments where chat_id = #{chatId}")
    void deleteByChat(Integer chatId);

    @Select("select id from tb_comments where chat_id = #{chatId}")
    List<Integer> getByChat(Integer chatId);

    // 删除二级评论
    @Delete("delete from tb_comments where root_comments_id = #{commentId}")
    int deleteByRootId(Integer commentId);

    // 删除评论
    @Delete("delete from tb_comments where id = #{commentId}")
    int deleteById(Integer commentId);
}
