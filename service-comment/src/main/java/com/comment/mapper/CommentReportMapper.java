package com.comment.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CommentReportMapper {

    @Insert("insert into tb_report_comments (userId, report_reason, comment_id, sub_comment_id, chat_id) VALUES " +
            "(#{userId},#{content},#{commentId},#{subCommentId},#{chatId})")
    void save(Integer userId, Integer commentId, String content,Integer subCommentId,Integer chatId);
}
