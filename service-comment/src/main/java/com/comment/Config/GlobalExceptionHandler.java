package com.comment.Config;


import Exception.*;
import constant.CodeConstant;
import constant.MsgConstant;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import result.Result;

/**
 * 全局异常处理器
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    // 最初异常,code = 500,msg="网络错误“
    @ExceptionHandler(BaseException.class)
    @ResponseBody
    public Result<String> handleException(Exception ex) {
        return Result.error(MsgConstant.NETWORK_ERROR, CodeConstant.NETWORK_ERROR);
    }

    // 评论不存在异常
    @ExceptionHandler(CommentsIsNotExistException.class)
    @ResponseBody
    public Result<String> CommentsIsNotExistException(CommentsIsNotExistException existException){
        return Result.error(MsgConstant.COMMENT_IS_EXIST,CodeConstant.COMMENT_IS_EXIST);
    }

    // 发布评论的人不存在
    @ExceptionHandler(UserNotExistException.class)
    @ResponseBody
    public Result<String> UserNotExistException(UserNotExistException existException){
        return Result.error(MsgConstant.USER_IS_NOT_EXIST,CodeConstant.USER_NOT_EXIST);
    }

    // 不是二级评论
    @ExceptionHandler(NotIsTwoCommentException.class)
    @ResponseBody
    public Result<String> NotIsTwoCommentException(NotIsTwoCommentException existException) {
        return Result.error(MsgConstant.PARAM_NOT_SATISFY, CodeConstant.PARAM_NOT_SATISFY);
    }
}
