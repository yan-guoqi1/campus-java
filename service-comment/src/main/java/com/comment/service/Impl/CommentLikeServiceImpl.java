package com.comment.service.Impl;

import DO.comments.LikeCommentsDO;
import com.comment.mapper.LikeCommentsMapper;
import com.comment.service.CommentLikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class CommentLikeServiceImpl implements CommentLikeService {

    @Autowired
    private LikeCommentsMapper likeCommentsMapper;

    // 用户点赞或者取消点赞 1表示修改为了点赞 0表示取消了点赞 null表示错误
    public Integer LikeComment(Integer userId, Integer commentId) {
        // 先查询是否存在点赞关系
        Integer isLike = likeCommentsMapper.getIsLike(userId, commentId);
        if (isLike==null){
            // 表示不存在点赞关系，用户即点赞
            LikeCommentsDO likeCommentsDO = new LikeCommentsDO();
            likeCommentsDO.setIsLike(1);
            likeCommentsDO.setCommentId(commentId);
            likeCommentsDO.setCreatedTime(LocalDateTime.now());
            likeCommentsDO.setUserId(userId);
            likeCommentsMapper.save(likeCommentsDO);
            return 1;
        }else if (isLike == 1){
            // 表示存在点赞关系，删除这个点赞行
            likeCommentsMapper.delete(userId,commentId);
            return 0;// 取消点赞
        } else if (isLike == 0) {
            // 表示踩，用户点赞那就将器修改未1
            likeCommentsMapper.updata(userId,commentId);
            return 1;
        }
        return null;
    }

    // 用户踩或者不踩评论 0表示修改为了踩 1表示取消了踩 null表示错误
    public Integer notLikeComment(Integer userId, Integer commentId) {
        // 先查询是否存在点赞关系
        Integer isLike = likeCommentsMapper.getIsLike(userId, commentId);
        if (isLike==null){
            // 表示不存在踩或者点赞关系，用户即踩
            LikeCommentsDO likeCommentsDO = new LikeCommentsDO();
            likeCommentsDO.setIsLike(0);
            likeCommentsDO.setCommentId(commentId);
            likeCommentsDO.setCreatedTime(LocalDateTime.now());
            likeCommentsDO.setUserId(userId);
            likeCommentsMapper.save(likeCommentsDO);
            return 0;
        }else if (isLike == 1){
            // 表示存在点赞关系，修改未踩
            likeCommentsMapper.updateNotLike(userId,commentId);
            return 0;// 取消点赞变为踩
        } else if (isLike == 0) {
            // 表示踩，用户再次点击那就删除踩这行记录
            likeCommentsMapper.delete(userId,commentId);
            return 1;
        }
        return null;
    }
}
