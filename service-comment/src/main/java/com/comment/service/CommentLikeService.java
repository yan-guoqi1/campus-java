package com.comment.service;

public interface CommentLikeService {
    // 用户点赞或者取消点赞评论
    Integer LikeComment(Integer userId, Integer commentId);

    // 用户踩或者不踩评论
    Integer notLikeComment(Integer userId, Integer commentId);
}
