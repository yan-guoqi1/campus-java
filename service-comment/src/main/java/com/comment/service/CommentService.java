package com.comment.service;

import DO.comments.CommentsDO;
import DTO.ReportCommentDTO;
import DTO.RootPageCommentDTO;
import DTO.TwoPageCommentDTO;
import VO.RootCommentVO;
import VO.TwoCommentVO;

import java.util.List;

public interface CommentService {
    void save(CommentsDO commentsDO);

    // 获取一级评论id
    List<Integer> getRootId(RootPageCommentDTO rootPageCommentDTO);

    // 获取一级评论具体消息
    RootCommentVO getRoot(Integer userId, Integer commentId);

    // 获取二级评论id数组
    List<Integer> getTwoId(TwoPageCommentDTO twoPageCommentDTO);

    // 获取二级评论的具体信息
    TwoCommentVO getTwo(Integer userId, Integer commentId);

    // 删除帖子相关的评论
    void MQdeleteByChat(Integer chatId);

    // 用户删除帖子
    void deleteComment(Integer userId,Integer commentId);

    // 用户举报
    void report(Integer integer, ReportCommentDTO reportCommentDTO);
}
