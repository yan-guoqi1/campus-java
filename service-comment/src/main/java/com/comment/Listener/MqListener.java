package com.comment.Listener;

import DO.comments.CommentsDO;
import com.comment.service.CommentService;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.units.qual.A;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Slf4j
@Component
public class MqListener {

    @Autowired
    private CommentService commentService;

    /**
     * 结束保存用户的评论数据,只接收 CommentsDO 的数据
     * @throws InterruptedException
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "chat.save.comments", durable = "true"),
            exchange = @Exchange(name = "ying.yan", type = ExchangeTypes.FANOUT)
    ))
    public void listenDirectQueue1(CommentsDO commentsDO) throws InterruptedException {
        commentService.save(commentsDO);
    }


    /**
     * 删除帖子评论数据
     * @throws InterruptedException
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = "chat.delete.comments", durable = "true"),
            exchange = @Exchange(name = "ying.yan.deleteComment", type = ExchangeTypes.FANOUT)
    ))
    public void listenDirectQueue2(Integer chatId) throws InterruptedException {
        log.info("删除帖子id为{}的评论数据",chatId);
        commentService.MQdeleteByChat(chatId);
    }
}
