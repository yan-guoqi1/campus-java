package com.comment.controller;

import com.comment.service.CommentLikeService;
import com.comment.service.CommentService;
import constant.CodeConstant;
import constant.MsgConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import result.Result;

@RestController
@RequestMapping("/comment")
public class LikeController {


    @Autowired
    private CommentLikeService commentLikeService;

    /**
     * 用户点赞或者取消点赞
     * @param userId 用户id
     * @param commentId 评论id
     * @return 1表示修改之后是点赞，0表示修改之后是取消点赞
     */
    @GetMapping("/LikeComment/{commentId}")
    public Result<Integer> LikeComment(@RequestHeader(name = "userId") String userId,
                                       @PathVariable(name = "commentId") Integer commentId){
        if (commentId<1||userId==null)
            return Result.error(MsgConstant.PARAM_NOT_SATISFY, CodeConstant.PARAM_NOT_SATISFY);
        return Result.success(commentLikeService.LikeComment(Integer.valueOf(userId),commentId));
    }

    /**
     * 用户踩或者不踩一个评论
     * @param userId 用户id
     * @param commentId 评论id
     * @return 是否还是踩
     */
    @GetMapping("/NotLikeComment/{commentId}")
    public Result<Integer> notLikeComment(@RequestHeader(name = "userId") String userId,
                                          @PathVariable(name = "commentId") Integer commentId){
        if (commentId<1||userId==null)
            return Result.error(MsgConstant.PARAM_NOT_SATISFY, CodeConstant.PARAM_NOT_SATISFY);
        return Result.success(commentLikeService.notLikeComment(Integer.valueOf(userId),commentId));
    }
}
