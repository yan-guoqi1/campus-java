package com.comment.controller;


import DTO.RootPageCommentDTO;
import DTO.TwoPageCommentDTO;
import VO.RootCommentVO;
import VO.TwoCommentVO;
import com.comment.service.CommentService;
import constant.CodeConstant;
import constant.MsgConstant;
import org.checkerframework.checker.units.qual.C;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import result.Result;

import java.util.List;

@RestController
@RequestMapping("/campus/comment")
public class CommentController {

    @Autowired
    private CommentService commentService;

    /**
     * 帖子回复的分页查询一级评论
     * @param rootPageCommentDTO 请求数据
     * 101 参数不满足要求
     * @return id数组
     */
    @PostMapping("/getRootComment")
    public Result<List<Integer>> RootPageComment(@RequestBody RootPageCommentDTO rootPageCommentDTO){
        // 1. 进行参数判断 ,参数不能为空,且id需要满足一定的条件
        if (rootPageCommentDTO.getNumber()==null||rootPageCommentDTO.getChatId()==null||rootPageCommentDTO.getLastCommentId()==null||
            rootPageCommentDTO.getNumber()<1    ||rootPageCommentDTO.getChatId()<1    ||rootPageCommentDTO.getLastCommentId()<0){
            return Result.error(MsgConstant.PARAM_NOT_SATISFY, CodeConstant.LACK_PARAM);
        }
        return Result.success(commentService.getRootId(rootPageCommentDTO));
    }


    /**
     * 根据评论id（一级）获取具体的评论消息
     * @param userId 用户id，可选
     * @param commentId 评论id
     * @return
     */
    @GetMapping("/getRootComment")
    public Result<RootCommentVO> getRootComment(@RequestParam(name = "userId",required = false) Integer userId,
                                                      @RequestParam("commentId") Integer commentId){
        if (commentId == null||commentId<1||(userId !=null&&userId<1)){
            return Result.error(MsgConstant.PARAM_NOT_SATISFY, CodeConstant.PARAM_NOT_SATISFY);
        }
        if (userId == null){
            // 表示游客登录,设置userId 为0
            userId = 0;
        }
        return Result.success(commentService.getRoot(userId,commentId));
    }


    /**
     * 获取二级评论的id数组
     * @return
     * 103 参数不满足要求
     */
    @PostMapping("/getTwoComment")
    public Result<List<Integer>> getTwoComment(@RequestBody TwoPageCommentDTO twoPageCommentDTO){
        if (twoPageCommentDTO.getRootCommentId() == null||twoPageCommentDTO.getRootCommentId()<1||
            twoPageCommentDTO.getLastCommentId()==null  ||twoPageCommentDTO.getLastCommentId()<0||
            twoPageCommentDTO.getNumber()<0){
            return Result.error(MsgConstant.PARAM_NOT_SATISFY,CodeConstant.PARAM_NOT_SATISFY);
        }
        return Result.success(commentService.getTwoId(twoPageCommentDTO));
    }


    /**
     * 根据二级评论id获取二级评论的具体信息
     * @param userId
     * @param commentId
     * @return
     */
    @GetMapping("/getTwoCommentById")
    public Result<TwoCommentVO> getTwoCommentById(@RequestParam(name = "userId",required = false) Integer userId,
                                                  @RequestParam("commentId") Integer commentId){
        if (commentId == null||commentId<1||(userId !=null&&userId<1)){
            return Result.error(MsgConstant.PARAM_NOT_SATISFY, CodeConstant.PARAM_NOT_SATISFY);
        }
        if (userId == null){
            // 表示游客登录,设置userId 为0
            userId = 0;
        }
        return Result.success(commentService.getTwo(userId,commentId));
    }





}
