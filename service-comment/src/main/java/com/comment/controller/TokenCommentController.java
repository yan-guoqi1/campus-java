package com.comment.controller;


import DTO.ReportCommentDTO;
import com.comment.service.CommentService;
import constant.CodeConstant;
import constant.MsgConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import result.Result;

@RestController
@RequestMapping("/comment/")
public class TokenCommentController {


    @Autowired
    private CommentService commentService;

    /**
     * 用户删除自己的评论
     * @param userId 用户id
     * @param commentId 评论id
     * @return 正确信息
     */
    @DeleteMapping("/deleteComment")
    public Result<String> deleteComment(@RequestHeader(name = "userId") String userId,
                                        @RequestParam("commentId") Integer commentId){

        if (commentId<1){
            return Result.error(MsgConstant.PARAM_NOT_SATISFY, CodeConstant.PARAM_NOT_SATISFY);
        }
        commentService.deleteComment(Integer.valueOf(userId),commentId);
        return Result.success();
    }

    /**
     * 用户举报
     * @param reportCommentDTO
     * @return
     */
    @PostMapping("/report")
    public Result<String> report(@RequestHeader(name = "userId") String userId,
                                 @RequestBody ReportCommentDTO reportCommentDTO){
        commentService.report(Integer.valueOf(userId),reportCommentDTO);
        return Result.success();
    }
}
