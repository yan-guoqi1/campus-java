package Exception;


/**
 * 不是二级评论
 */
public class NotIsTwoCommentException extends BaseException{
    public NotIsTwoCommentException() {
    }

    public NotIsTwoCommentException(String msg) {
        super(msg);
    }
}
