package Exception;


/**
 * 二人没有关注关系
 */
public class NOFollowException extends BaseException{
    public NOFollowException() {
    }

    public NOFollowException(String msg) {
        super(msg);
    }
}
