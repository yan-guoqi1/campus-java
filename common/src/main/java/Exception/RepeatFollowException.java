package Exception;


/**
 * 重复关注异常
 */
public class RepeatFollowException extends BaseException{
    public RepeatFollowException() {
    }

    public RepeatFollowException(String msg) {
        super(msg);
    }
}
