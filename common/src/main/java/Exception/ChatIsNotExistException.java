package Exception;


/**
 * 帖子不存在
 */
public class ChatIsNotExistException extends BaseException{
    public ChatIsNotExistException() {
    }

    public ChatIsNotExistException(String msg) {
        super(msg);
    }
}
