package Exception;


/**
 * 帖子已经点赞
 */
public class NotLikeChatException extends BaseException{
    public NotLikeChatException() {
    }

    public NotLikeChatException(String msg) {
        super(msg);
    }
}
