package Exception;


/**
 * 图片格式不正确
 */
public class ImageIsNotRightException extends BaseException{
    public ImageIsNotRightException() {
    }

    public ImageIsNotRightException(String msg) {
        super(msg);
    }
}
