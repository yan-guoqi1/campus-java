package Exception;


/**
 * 用户不存在异常
 */
public class UserNotExistException extends BaseException{
    public UserNotExistException() {
    }

    public UserNotExistException(String msg) {
        super(msg);
    }
}
