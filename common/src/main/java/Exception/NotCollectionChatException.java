package Exception;


/**
 * 帖子已经收藏
 */
public class NotCollectionChatException extends BaseException{
    public NotCollectionChatException() {
    }

    public NotCollectionChatException(String msg) {
        super(msg);
    }
}
