package Exception;


/**
 * 评论不存在
 */
public class CommentsIsNotExistException extends BaseException{
    public CommentsIsNotExistException() {
    }

    public CommentsIsNotExistException(String msg) {
        super(msg);
    }
}
