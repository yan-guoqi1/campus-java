package Exception;


/**
 * 密码不正确
 */
public class PasswordIsNOTRightException extends BaseException{
    public PasswordIsNOTRightException() {
    }

    public PasswordIsNOTRightException(String msg) {
        super(msg);
    }
}
