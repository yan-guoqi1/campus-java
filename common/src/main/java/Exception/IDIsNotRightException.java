package Exception;


/**
 * id不正确
 */
public class IDIsNotRightException extends BaseException{
    public IDIsNotRightException() {
    }

    public IDIsNotRightException(String msg) {
        super(msg);
    }
}
