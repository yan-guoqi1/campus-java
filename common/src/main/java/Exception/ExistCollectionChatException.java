package Exception;


/**
 * 帖子已经收藏
 */
public class ExistCollectionChatException extends BaseException{
    public ExistCollectionChatException() {
    }

    public ExistCollectionChatException(String msg) {
        super(msg);
    }
}
