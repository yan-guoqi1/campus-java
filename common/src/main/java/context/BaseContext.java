package context;

/**
 * 当前线程存储用户id
 * 在Spring Boot这样的Java Web项目中，用户的每一次请求通常由一个独立的线程处理，
 * 当请求结束时，分配给该请求的线程会被销毁。这是多线程模型的基本原理。
 */
public class BaseContext {

    public static ThreadLocal<Integer> threadLocal = new ThreadLocal<>();

    public static void setCurrentId(Integer id) {
        threadLocal.set(id);
    }

    public static Integer getCurrentId() {
        return threadLocal.get();
    }

    public static void removeCurrentId() {
        threadLocal.remove();
    }
}
