package result;

import lombok.Data;


/**
 * 通用返回结果，服务端响应的数据最终都会封装成此对象
 * @param <T>
 */
@Data
public class Result<T> {

    //编码：200成功，0和其它数字为失败
    private Integer code;

    //错误信息
    private String msg;

    //数据
    private T data;

    /**
     * 带返回数据的请求成功
     * @param object 数据
     * @return 返回结果
     * @param <T> 数据类型
     */
    public static <T> Result<T> success(T object) {
        Result<T> result = new Result<T>();
        result.data = object;
        result.code = 200;
        return result;
    }

    /**
     * 不带返回结果的结果对象
     * @return 只有状态码的返回结果
     * @param <T> 数据类型（无用）
     */
    public static <T> Result<T> success() {
        Result<T> result = new Result<T>();
        result.code = 200;
        return result;
    }

    /**
     * 错误的返回结果，带消息
     * @param msg 错误消息
     * @return 返回结果
     * @param <T> 数据类型
     */
    public static <T> Result<T> error(String msg) {
        Result<T> result = new Result<T>();
        result.msg = msg;
        result.code = 0;
        return result;
    }

    /**
     * 带状态码的返回结果
     * @param msg 错误消息
     * @param code 错误状态码
     * @return 返回结果
     * @param <T> 数据类型 无用
     */
    public static <T> Result<T> error(String msg, Integer code) {
        Result<T> result = new Result<T>();
        result.msg = msg;
        result.code = code;
        return result;
    }
}
