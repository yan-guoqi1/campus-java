package result;

import lombok.Data;

/**
 * 通用返回结果，服务端响应的数据最终都会封装成此对象
 * @param <T>
 */
@Data
public class ResultToLogo {

    private Integer code; //编码：200成功，0和其它数字为失败

    private String msg; //错误信息

    private String data; //数据

    private Integer logoId; // 需要签到的id
}
