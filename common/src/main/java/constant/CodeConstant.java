package constant;

/**
 * 状态码常量
 */
public class CodeConstant {
    /**
     * 登录阶段
     * 1~50
     */
    // 重复获取验证码
    public static final Integer REPEATEDLY_GET_CODE = 10;
    // 验证码过期了
    public static final Integer CODE_EXPIRATION = 11;
    // 验证码不正确
    public static final Integer CODE_NOT_RIGHT = 12;
    // 密码错误
    public static final Integer PASSWORD_NOT_RIGHT = 13;
    // 用户不存在
    public static final Integer USER_NOT_EXIST = 14;

    // token 不存在
    public static final Integer TOKEN_IS_EMPTY = 20;
    // token 解析失败
    public static final Integer TOKEN_PARSING_FAILED = 21;


    /**
     * 文件-图片处理阶段 51 - 99
     */
    public static final Integer IMAGE_FORMAT_NOT_RIGHT = 51;


    /**
     * 参数阶段 100-200
     */

    // 缺少参数
    public static final Integer LACK_PARAM = 101;

    // 电话号码不正确
    public static final Integer PHONE_NUMBER_NOT_RIGHT = 102;

    // 参数不满足要求
    public static final Integer PARAM_NOT_SATISFY = 103;

    // id 不符合要求
    public static final Integer ID_IS_NOT_RIGHT = 105;

    // 密码不符合标准
    public static final Integer PASSWORD_IS_SATISFY = 106;


    /**
     * 用户阶段201-250
     */
    public static final Integer USER_IS_NOT_EXIST = 201;
    public static final Integer REPEAT_FOLLOW = 202;
    public static final Integer USER_NO_FOLLOW = 203;


    /**
     * 帖子阶段
     */

    // 帖子不存在
    public static final Integer CHAT_IS_EXIST = 301;

    // 重复收藏
    public static final Integer REPEAT_COLLECTION = 302;

    // 重复点赞
    public static final Integer REPEAT_LIKE = 303;

    // 没有收藏关系
    public static final Integer NOT_COLLECTION = 304;

    // 没有点赞关系
    public static final Integer NOT_LIKE = 305;

    // 评论不存在
    public static final Integer COMMENT_IS_EXIST = 306;




    // 异常处理
    public static final Integer NETWORK_ERROR = 500;
}
