package constant;


/**
 * redis中的key的前缀
 */
public class RedisPreKeyConstant {

    /**
     * 全称的部分
     */

    // 一级地点 全称
    public static final String CITY_FIRST = "city:first";

    // 举报标签
    public static final String REPORT_SIGN="report_chat_sign";

    // 一天以内的帖子
    public static final String RANKING_DAY="Ranking:day";

    // 一周以内的帖子
    public static final String RANKING_WEEK="Ranking:week";

    // 一个月以内的帖子
    public static final String RANKING_MONTH="Ranking:MONTH";





    /**
     * 前缀的部分
     */

    // 用户登录
    public static final String USER_LOGIN = "login:code:";

    // 二级地点 部分
    public static final String CITY_SECOND = "city:second:";

    // 新用户设置密码
    public static final String USER_SET_PASSWORD = "newUser:setPassword:";

    // 帖子保存（不带用户信息）
    public static final String CHAT_MSG = "chat:byId:";
}
