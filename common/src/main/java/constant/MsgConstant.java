package constant;


/**
 * 消息常量
 */
public class MsgConstant {

    // 通用使用的常量
    public static final String LACK_PARAM = "缺少必要的参数";
    public static final String PARAM_NOT_SATISFY = "参数不满足要求";
    public static final String NETWORK_ERROR = "网络错误";

    // 单独使用的常量
    public static final String TOKEN_IS_EMPTY = "token是空的";
    public static final String TOKEN_PARSING_FAILED = "token解析失败";

    // 登录阶段
    public static final String REPEATEDLY_GET_CODE = "重复获取验证码";
    public static final String CODE_EXPIRATION = "验证码过期了";
    public static final String CODE_NOT_RIGHT = "验证码不正确";
    public static final String PHONE_NUMBER_NOT_RIGHT = "电话号码不正确";
    public static final String PASSWORD_NOT_RIGHT = "密码不正确";
    public static final String USER_NOT_EXIST = "用户不存在";

    // 特殊参数阶段
    public static final String USERNAME_OVER_TEN = "用户昵称超过10位";
    public static final String USER_SET_ONLY_0_AND_1 = "性别只能为0或者1";
    public static final String ID_IS_NOT_RIGHT = "id是不符合要求的";
    public static final String PASSWORD_IS_SATISFY = "密码不符合标准";

    // 图片阶段
    public static final String IMAGE_FORMAT_NOT_RIGHT = "图片格式不正确";

    // 用户阶段
    public static final String USER_IS_NOT_EXIST = "用户不存在";
    public static final String REPEAT_FOLLOW = "重复关注";
    public static final String USER_NO_FOLLOW = "没有关注关系";

    // 帖子阶段
    public static final String CHAT_IS_EXIST = "帖子不存在";
    public static final String REPEAT_COLLECTION = "重复收藏帖子";
    public static final String REPEAT_LIKE = "重复点赞帖子";
    public static final String NOT_COLLECTION = "没有收藏关系";
    public static final String NOT_LIKE = "没有点赞关系";
    public static final String COMMENT_IS_EXIST = "评论不存在";
}
