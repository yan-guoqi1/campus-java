package com.user;

import com.feign.clients.ChatClient;
import com.feign.clients.UserClient;
import com.feign.clients.commentClient;
import com.feign.config.DefaultFeignConfiguration;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@MapperScan("com.user.mapper")
@EnableFeignClients(clients = {ChatClient.class, commentClient.class},defaultConfiguration = DefaultFeignConfiguration.class)
public class UserApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserApplication.class, args);
    }
}
