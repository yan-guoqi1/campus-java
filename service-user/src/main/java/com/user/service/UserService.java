package com.user.service;

import DTO.EditPasswordDTO;
import DTO.UserLoginDTO;
import VO.EditUserMsgVO;
import VO.Feight.ChatByUserMsgVO;
import VO.Feight.CommentUserMsgVO;
import VO.UserMsgVO;
import result.Result;

public interface UserService {
    //密码登录
    Integer LoginByPassword(UserLoginDTO userLoginDTO);

    // 验证码登录，获取用户id
    Integer userRegistered(UserLoginDTO userLoginDTO);

    // 保存用户头像
    void saveAvatar(String path, Integer userId);

    // 获取头像文件地址
    String getAvatar(String userId);

    // 获取用户信息
    UserMsgVO getUserMsg(String userId);

    // 修改用户信息
    void editUserMsg(String userId, EditUserMsgVO editUserMsgVO);

    // feign调用判断用户是否存在
    boolean getUserExist(Integer id);

    // 判断用户的密码类型
    Integer getPasswordType(Integer userId);

    String getUserPhone(Integer userId);

    // 保存用户密码
    void savePassword(String newPassword, Integer userId);

    // 用户修改密码
    void updatePassword(EditPasswordDTO editPasswordDTO, Integer userId);

    // 获取帖子信息时返回的用户信息
    Result<ChatByUserMsgVO> getChatUserMsg(Integer chatUserId, Integer userId);

    // 获取用户的头像以及昵称
    Result<CommentUserMsgVO> getCommentUserMsg(Integer userId);
}
