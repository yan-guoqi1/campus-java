package com.user.service.Impl;


import DO.user.FansDO;
import DO.user.UserDO;
import VO.FansVO;
import VO.FollowVO;
import com.user.mapper.FensMapper;
import com.user.mapper.UserMapper;
import com.user.service.FensAndFollowService;
import Exception.UserNotExistException;
import Exception.RepeatFollowException;
import Exception.NOFollowException;
import com.utils.ImageUtils;
import com.utils.StringUtils;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 用户粉丝和关注表
 */
@Service
public class FensAndFollowServiceImpl implements FensAndFollowService {

    @Autowired
    private FensMapper fensMapper;

    @Autowired
    private UserMapper userMapper;

    /**
     * 用户关注
     * @param ByFollowId 被关注者id
     * @param userId 关注者id
     */
    public void follow(Integer ByFollowId, Integer userId) {
        // 1. 查询这个数据库中有无这个人
        if (userMapper.getIsExist(ByFollowId) == 0){
            // 表示数据库中，没有这个人,抛出异常
            throw new UserNotExistException("数据库中没有这个人");
        }
        LocalDateTime time = LocalDateTime.now();

        // 2. 查询关注表是否存在二人的关注信息
        if (fensMapper.getIsFollow(ByFollowId,userId)!=0){
            // 表示二人存在关注信息，不能进行关注
            throw new RepeatFollowException("重复关注");
        }

        // 3. 补充粉丝表
        fensMapper.save(ByFollowId,userId,time);
    }


    /**
     * 取消关注
     * @param ByFollowId 关注者id
     * @param userId 用户id
     */
    public void cancelFollow(Integer ByFollowId, Integer userId) {
        if (fensMapper.delete(ByFollowId,userId) == 0){
            // 表示在删除关注关系的时候这二人没有关注信息
            throw new NOFollowException();
        }
    }

    /**
     * 获取用户粉丝信息
     * @param userId 请求者id
     * @param userByFansId 获取哪个用户的粉丝信息
     * @return
     */
    public List<FansVO> getUserFansByUserId(Integer userId, Integer userByFansId) {
        List<FansDO> fansDOS = fensMapper.getById(userByFansId);
        List<FansVO> fansVOList = new ArrayList<>();
        for (FansDO fansDO : fansDOS) {
            FansVO fansVO = new FansVO();
            fansVO.setFansId(fansDO.getFansId());
            // 根据粉丝用户id查询该粉丝的具体信息
            UserDO userDO = userMapper.getById(fansDO.getFansId());

            fansVO.setUserName(userDO.getUserName());
            fansVO.setAvatar(StringUtils.extractFilename(userDO.getAvatar()));
            fansVO.setDescription(userDO.getDescription());

            // 根据请求者id获取关注关系
            if (userId == 0){
                fansVO.setFollow(0);
            } else if (Objects.equals(userByFansId, userId)) {
                // 查询自己的粉丝用户,只需要判断是否是朋友关系
                fansVO.setFollow(0);
                if (fensMapper.getIsFollow(fansDO.getFansId(),userId) == 1){
                    fansVO.setFollow(-1);
                }
            } else {
                // 查询请求者与该粉丝的关注信息 1：请求者已经关注 0 请求者未关注(包含粉丝已经关注或者没有关注请求者) -1 请求者与该粉丝是互关，朋友关系
                int isFollow = fensMapper.getIsFollow(fansDO.getFansId(), userId);
                int isfens = fensMapper.getIsFollow(userId, fansDO.getFansId());
                if (isFollow == 1&& isfens == 1){
                    // 朋友关系
                    fansVO.setFollow(-1);
                } else if (isFollow == 1) {
                    // 请求者已经关注
                    fansVO.setFollow(1);
                }else {
                    // 请求者未关注
                    fansVO.setFollow(0);
                }
            }
            fansVOList.add(fansVO);
        }
        return fansVOList;
    }

    /**
     * 获取用户关注信息
     * @param userId 请求者id
     * @param userByFollowId 获取那个用户的关注信息
     * @return
     */
    public List<FollowVO> getUserFollowByUserId(Integer userId, Integer userByFollowId) {

        List<FollowVO> followVOList = new ArrayList<>();
        List<FansDO> fansDOList = fensMapper.getByFollow(userByFollowId);
        for (FansDO fansDO : fansDOList) {
            FollowVO followVO = new FollowVO();
            followVO.setFollowId(fansDO.getUserId());
            // 根据这个关注者id获取他的具体信息

            UserDO userDO = userMapper.getById(fansDO.getUserId());
            followVO.setUserName(userDO.getUserName());
            followVO.setAvatar(userDO.getAvatar());
            followVO.setDescription(userDO.getDescription());

            // 根据请求者id获取关注信息
            /* 与请求者的关注关系。1：请求者已经关注 0 请求者未关注(包含粉丝已经关注或者没有关注请求者) -1 请求者与该粉丝是互关，朋友关系 */
            if (userId == 0){
                // 游客登录
                followVO.setFollow(0);
            } else if (userId.equals(userByFollowId)) {
                // 本人访问，子需要判断是否是朋友关系
                followVO.setFollow(1);
                if (fensMapper.getIsFollow(userByFollowId,fansDO.getUserId()) == 1){
                    followVO.setFollow(-1);
                }
            }else {
                int isFollow = fensMapper.getIsFollow(fansDO.getFansId(), userId);
                int isfens = fensMapper.getIsFollow(userId, fansDO.getFansId());

                if (isFollow == 1&& isfens ==1){
                    followVO.setFollow(-1);
                } else if (isFollow ==1) {
                    followVO.setFollow(1);
                }else {
                    followVO.setFollow(0);
                }
            }
            followVOList.add(followVO);
        }

        return followVOList;
    }
}
