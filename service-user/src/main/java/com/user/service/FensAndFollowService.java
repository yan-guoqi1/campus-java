package com.user.service;

import VO.FansVO;
import VO.FollowVO;

import java.util.List;

public interface FensAndFollowService {

    // 用户去关注一个人
    void follow(Integer ByFollowId, Integer userId);

    // 用户取消关注
    void cancelFollow(Integer ByFollowId, Integer userId);

    //
    List<FansVO> getUserFansByUserId(Integer userId,Integer userByFansId);

    List<FollowVO> getUserFollowByUserId(Integer userId, Integer userByFollowId);
}
