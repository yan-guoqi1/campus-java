package com.user.service;

import DO.user.CityDO;
import DO.user.ProvincialDO;

import java.util.List;

public interface CityService {

    // 获取一级地名
    List<ProvincialDO> getFirstCity();

    // 获取二级标题
    List<CityDO> getSecondCity(Integer id);
}
