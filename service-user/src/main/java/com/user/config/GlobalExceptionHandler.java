package com.user.config;


import constant.CodeConstant;
import constant.MsgConstant;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import result.Result;
import Exception.*;
/**
 * 全局异常处理器
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    // 最初异常,code = 500,msg="网络错误“
    @ExceptionHandler(BaseException.class)
    @ResponseBody
    public Result<String> handleException(Exception ex) {
        return Result.error(MsgConstant.NETWORK_ERROR, CodeConstant.NETWORK_ERROR);
    }

    // 用户不存在异常
    @ExceptionHandler(UserNotExistException.class)
    @ResponseBody
    public Result<String> UserNotExistException(UserNotExistException existException){
        return Result.error(MsgConstant.USER_IS_NOT_EXIST,CodeConstant.USER_IS_NOT_EXIST);
    }


    // 重复关注异常
    @ExceptionHandler(RepeatFollowException.class)
    @ResponseBody
    public Result<String> RepeatFollowException(RepeatFollowException ex){
        return Result.error(MsgConstant.REPEAT_FOLLOW,CodeConstant.REPEAT_FOLLOW);
    }

    // 没有关注关系
    @ExceptionHandler(NOFollowException.class)
    @ResponseBody
    public Result<String> NOFollowException(NOFollowException ex){
        return Result.error(MsgConstant.USER_NO_FOLLOW,CodeConstant.USER_NO_FOLLOW);
    }

    // id 不符合要求
    @ExceptionHandler(IDIsNotRightException.class)
    @ResponseBody
    public Result<String> IDIsNotRightException(IDIsNotRightException ex){
        return Result.error(MsgConstant.ID_IS_NOT_RIGHT,CodeConstant.ID_IS_NOT_RIGHT);
    }

    // 密码不正确异常
    @ExceptionHandler(PasswordIsNOTRightException.class)
    @ResponseBody
    public Result<String> PasswordIsNOTRightException(PasswordIsNOTRightException ex){
        return Result.error(MsgConstant.PASSWORD_NOT_RIGHT,CodeConstant.PASSWORD_NOT_RIGHT);
    }
}
