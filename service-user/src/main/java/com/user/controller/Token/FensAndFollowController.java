package com.user.controller.Token;

import com.user.service.FensAndFollowService;
import constant.CodeConstant;
import constant.MsgConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import result.Result;

import java.util.Objects;

@RestController
@RequestMapping("/user")
public class FensAndFollowController {

    @Autowired
    private FensAndFollowService fensAndFollowService;

    /**
     * 用户关注一个人
     * @param userId 关注者id
     * @param id 用户id
     * @return 成功对象
     * 500 网络错误，实际上是关注者和自己是一个人
     * 201 没有这个人
     * 202 重复关注
     */
    @GetMapping("/follow/{id}")
    public Result<String> Follow(@RequestHeader(name = "userId") String userId,@PathVariable("id") Integer id){
        if (Objects.equals(id, Integer.valueOf(userId))){
            // 表示关注者和自己是一个id
            return Result.error(MsgConstant.NETWORK_ERROR, CodeConstant.NETWORK_ERROR);
        }
        fensAndFollowService.follow(id,Integer.valueOf(userId));
        return Result.success();
    }


    /**
     * 取消关注
     * @param userId 关注者id
     * @param id 用户id
     * @return
     * 203  没有关注关系
     */
    @GetMapping("/CancelFollow/{id}")
    public Result<String> CancelFollow(@RequestHeader(name = "userId") String userId,@PathVariable("id") Integer id){
        fensAndFollowService.cancelFollow(id,Integer.valueOf(userId));
        return Result.success();
    }
}
