package com.user.controller.User;

import VO.FansVO;
import VO.FollowVO;
import com.user.service.FensAndFollowService;
import constant.CodeConstant;
import constant.MsgConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import result.Result;

import java.util.List;


/**
 * 用户权限控制类，用户可以判断下面接口是否可以让其他人看到
 */
@RestController
@RequestMapping("/campus/user")
public class UserQController {

    @Autowired
    private FensAndFollowService fensAndFollowService;

    /**
     * 获取用户粉丝数据
     * @param userId 请求人id
     * @param userByFansId 获取那个用户的粉丝
     * @return 粉丝数据
     */
    @GetMapping("/getUserFansByUserId/{userByFansId}")
    public Result<List<FansVO>> getUserFansByUserId(@RequestHeader(name = "userId",required = false) String userId,
                                                    @PathVariable(name = "userByFansId") Integer userByFansId){
        if (userByFansId<1){
            return Result.error(MsgConstant.PARAM_NOT_SATISFY, CodeConstant.PARAM_NOT_SATISFY);
        }
        return Result.success(fensAndFollowService.getUserFansByUserId(Integer.valueOf(userId==null?"0":userId),userByFansId));
    }


    /**
     * 获取用户关注数据
     * @param userId 请求人id
     * @param userByFollowId 获取那个用户的关注
     * @return 关注数据
     */
    @GetMapping("/getUserFollowByUserId/{userByFollowId}")
    public Result<List<FollowVO>> getUserFollowByUserId(@RequestHeader(name = "userId",required = false) String userId,
                                                      @PathVariable(name = "userByFollowId") Integer userByFollowId){
        if (userByFollowId<1){
            return Result.error(MsgConstant.PARAM_NOT_SATISFY, CodeConstant.PARAM_NOT_SATISFY);
        }
        return Result.success(fensAndFollowService.getUserFollowByUserId(Integer.valueOf(userId==null?"0":userId),userByFollowId));
    }
}
