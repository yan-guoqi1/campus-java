package com.user.controller.Campus;

import DO.user.CityDO;
import DO.user.ProvincialDO;
import com.user.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import result.Result;

import java.util.List;

/**
 * 获取城市名称的类
 */
@RestController
@RequestMapping("/campus/user/city")
public class CityController {


    @Autowired
    private CityService cityService;

    /**
     * 获取一级地名
     * @return 地名
     */
    @GetMapping("/getFirstCity")
    public Result<List<ProvincialDO>> getFirstCity(){
        return Result.success(cityService.getFirstCity());
    }

    /**
     * 获取二级地名
     * @param id 一级地名id
     * @return 二级地名id
     */
    @GetMapping("/getSecondCity/{id}")
    public Result<List<CityDO>> getSecondCity(@PathVariable Integer id){
        return Result.success(cityService.getSecondCity(id));
    }
}
