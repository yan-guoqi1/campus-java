package com.user.controller.Feign;

import VO.Feight.ChatByUserMsgVO;
import VO.Feight.CommentUserMsgVO;
import com.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import result.Result;

/**
 * 微服务调用类，用户服务之间的调用，不对外开放
 */
@RestController
@RequestMapping("/feign/user/")
public class FeignUserController {

    @Autowired
    private UserService userService;

    /**
     * 判断用户是否存在
     * @param id 用户id
     * @return 是否存在
     */
    @GetMapping("/{id}")
    public boolean findById(@PathVariable("id") Integer id){
        return userService.getUserExist(id);
    }


    /**
     * 获取基本的用户信息
     * @param chatUserId
     * @param userId
     * @return
     */
    @GetMapping("/getChatUserMsg")
    public Result<ChatByUserMsgVO> getChatUserMsg(@RequestParam("chatUserId") Integer chatUserId,
                                                 @RequestParam("userId")Integer userId){
        return userService.getChatUserMsg(chatUserId,userId);
    }

    /**
     * 获取用户的头像以及昵称消息
     * @return
     */
    @GetMapping("/getCommentUserMsg/{userId}")
    public Result<CommentUserMsgVO> getCommentUserMsg(@PathVariable Integer userId){
        return userService.getCommentUserMsg(userId);
    }
}
