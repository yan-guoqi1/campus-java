package com.user.mapper;

import DO.user.UserDO;
import VO.UserMsgVO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface UserMapper {

    // 根据手机号查询用户
    @Select("select * from tb_user where phone_number = #{phoneNum}")
    UserDO getUser(String phoneNum);

    // 用户注册
    void insert(UserDO userDO);

    // 保存用户头像
    @Update("update tb_user set avatar = #{path} where id = #{userId}")
    void saveAvatar(String path, Integer userId);

    // 获取头像地址
    @Select("select avatar from tb_user where id = #{userId}")
    String getAvatar(String userId);

    // 获取用户信息
    @Select("select id, phone_number, user_name, sex, description, year,month, day, first_place, second_place, created_time,hobby from tb_user where id = #{userId}")
    UserMsgVO getMsg(String userId);

    // 编辑用户信息
    void editUserMsg(UserDO userDO);

    // 查询数据库是否存在这个人
    @Select("select COUNT(*) from tb_user where id = #{byFollowId}")
    int getIsExist(Integer byFollowId);

    // 根据用户id获取用户信息
    @Select("select * from tb_user where id = #{userId}")
    UserDO getById(Integer userId);

    @Select("select phone_number from tb_user where id= #{userId}")
    String getUserPhone(Integer userId);

    // 用户更新或者保存密码
    @Update("update tb_user set salt = #{salt}, password = #{password} where id = #{userId}")
    void savePassword(String salt, String password, Integer userId);
}
