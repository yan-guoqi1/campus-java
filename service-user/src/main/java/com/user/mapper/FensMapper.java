package com.user.mapper;

import DO.user.FansDO;
import VO.FollowVO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDateTime;
import java.util.List;

@Mapper
public interface FensMapper {

    @Insert("insert into tb_fans (user_id, fans_id, created_time) VALUES (#{byFollowId},#{userId},#{time})")
    void save(Integer byFollowId, Integer userId, LocalDateTime time);

    @Delete("delete from tb_fans where user_id = #{byFollowId} and fans_id = #{userId}")
    int delete(Integer byFollowId, Integer userId);

    @Select("SELECT COUNT(*) from tb_fans where user_id = #{byFollowId} and fans_id = #{userId}")
    int getIsFollow(Integer byFollowId, Integer userId);

    // 获取用户粉丝数
    @Select("select COUNT(*) from tb_fans where user_id = #{userId}")
    Integer getFensNumber(String userId);

    // 获取用户关注数
    @Select("select COUNT(*) from tb_fans where fans_id = #{userId}")
    Integer getFollowNumber(String userId);

    @Select("select * from tb_fans where user_id = #{userByFansId}")
    List<FansDO> getById(Integer userByFansId);

    @Select("select * from tb_fans where fans_id = #{userByFollowId}")
    List<FansDO> getByFollow(Integer userByFollowId);
}
