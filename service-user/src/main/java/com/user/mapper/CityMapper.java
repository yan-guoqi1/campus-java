package com.user.mapper;

import DO.user.CityDO;
import DO.user.ProvincialDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CityMapper {
    // 获取一级地名
    @Select("select * from provincial")
    List<ProvincialDO> getFirstCity();


    // 根据一级地名id获取二级地名
    @Select("select * from city where pid = #{id}")
    List<CityDO> getSecondCity(Integer id);
}
