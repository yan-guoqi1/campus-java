package com.utils;


import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * 操作密码加盐以及加盐操作
 */
public class PasswordUtil {

    /**
     * 加密算法---SHA-256加密
     * @param salt 盐
     * @param password 用户密码
     * @return 加密之后的字符串 一个固定大小（256位或32字节）的哈希值
     */
    public static String getSHA256Hash(String salt,String password) {
        try {
            // 创建一个MessageDigest实例并指定SHA-256算法
            MessageDigest digest = MessageDigest.getInstance("SHA-256");

            // 使用指定的字节更新摘要
            byte[] hash = digest.digest((password + salt).getBytes(StandardCharsets.UTF_8));

            // 将字节转换为十六进制字符串
            StringBuilder hexString = new StringBuilder();
            for (byte b : hash) {
                String hex = Integer.toHexString(0xff & b);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 生成指定长度为10的随机盐字符串。
     *
     * @return 生成的随机盐字符串
     */
    public static String generateRandomSalt() {
        SecureRandom random = new SecureRandom();
        StringBuilder salt = new StringBuilder();
        while (salt.length() < 10) {
            // 生成一个随机字节，并转换为两位的十六进制字符
            byte[] bytes = new byte[1];
            random.nextBytes(bytes);
            salt.append(String.format("%02x", bytes[0]));
        }
        return salt.toString();
    }

    /**
     * 密码标准
     * @param password 密码字符串
     * @return 是否符合要求
     */
    public static boolean isValidPassword(String password) {
        // 1. 密码不能为空
        if (password == null || password.isEmpty()) {
            return false;
        }

        // 2. 密码长度必须大于8位
        if (password.length() <= 8) {
            return false;
        }

        // 4. 密码长度小于25位
        if (password.length() >= 25) {
            return false;
        }

        // 3. 密码必须包含字母和数字
        boolean hasLetter = false;
        boolean hasDigit = false;
        for (char c : password.toCharArray()) {
            if (Character.isLetter(c)) {
                hasLetter = true;
            } else if (Character.isDigit(c)) {
                hasDigit = true;
            }
        }

        return hasLetter && hasDigit;
    }

}
