package com.utils;


import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * 排行榜相关工具类
 */
public class RankingUtils {

    /**
     * 计算原始分数
     * @param collectionNumber 收藏数
     * @param likeNumber 点赞数
     * @param commentNumber 回复数
     * @return 原始分数
     */
    public static double getOriginalScore(Integer collectionNumber,Integer likeNumber,Integer commentNumber){
        return collectionNumber + 3*likeNumber + 5* commentNumber;
    }


    /**
     * 判断该帖子是否推出排行
     * @param dateTime 帖子创建的秒数
     * @param numberDays 天数
     * @return 是否结束排行， true 没有结束 false 结束排行时间
     */
    public static boolean secondsRemaining(Long dateTime, int numberDays) {

        // 获取当前系统的默认时区 将LocalDateTime与当前系统时区结合
        ZonedDateTime zonedDateTime = LocalDateTime.now().atZone(ZoneId.systemDefault());

        // 获取自Epoch以来的秒数
        long seconds = zonedDateTime.toEpochSecond();

        // 计算结束排行秒数
        long endSeconds = dateTime + (long) numberDays * 24 * 60 * 60;

        return endSeconds>seconds;
    }

    /**
     * 将时间变量转化为秒数
     * @param dataTime 时间参数
     * @return 秒数
     */
    public static Long getSeconds(LocalDateTime dataTime) {

        // 获取当前系统的默认时区
        ZoneId zoneId = ZoneId.systemDefault();

        // 将LocalDateTime与当前系统时区结合
        ZonedDateTime zonedDateTime = dataTime.atZone(zoneId);

        // 获取自Epoch以来的秒数
        return zonedDateTime.toEpochSecond();
    }


}
