package com.utils;


import java.io.File;

/**
 * 对字符串加工的类
 */
public class StringUtils {

    // 获取地址的名称
    public static String extractFilename(String filePath) {
        // 确保filePath不为空
        if (filePath == null) {
            return null;
        }

        // 找到最后一个反斜杠的位置
        int lastIndex = filePath.lastIndexOf(File.separator);

        // 如果找到了反斜杠，则返回其后的所有字符
        if (lastIndex != -1) {
            return filePath.substring(lastIndex + 1);
        }

        // 如果没有找到反斜杠，则可能整个字符串就是文件名
        return filePath;
    }
}
