package com.utils;

import java.time.LocalDate;
import java.time.Period;

public class AgeCalculator {

    public static Integer calculateAge(Integer year, Integer month, Integer day) {
        // 获取当前日期  
        LocalDate currentDate = LocalDate.now();
        // 构建出生日期  
        LocalDate birthDate = LocalDate.of(year, month, day);

        // 检查出生日期是否在当前日期之前  
        if (birthDate.isAfter(currentDate) || birthDate.isEqual(currentDate)) {
            throw new IllegalArgumentException("出生日期不能在当前日期之后或与其相同");
        }

        // 计算年龄  
        Period period = Period.between(birthDate, currentDate);
        return period.getYears();
    }
}