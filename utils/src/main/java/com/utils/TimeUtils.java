package com.utils;

import java.time.Duration;
import java.time.LocalDateTime;

/**
 * 对时间进行处理的类
 */
public class TimeUtils {

    /**
     * 判断时间是几小时前还是几分钟几天前
     * @param creationTime
     * @return
     */
    public static String formatTimeAgo(LocalDateTime creationTime) {
        LocalDateTime now = LocalDateTime.now();
        Duration duration = Duration.between(creationTime, now);

        // 计算秒数
        long seconds = duration.getSeconds();
        long minutes = seconds / 60;
        long hours = seconds / (60 * 60);
        long days = seconds / (24 * 60 *60);

        // 1. 两分钟之内显示刚刚
        if (seconds <= 120L){
            return "刚刚";
        }

        // 2. 一个小时之类显示分钟数
        if (seconds <= 60 * 60L){
            // eg: 5分钟前  20分钟前
            return (seconds / 60) + "分钟前";
        }

        // 3. 24 个小时以内，判断是否过了一个天数，如果过了一个天数，就显示 昨天 + 小时+分钟。如果没有就显示具体小时数
        if (seconds <= 60*60*24L){
            if (creationTime.equals(now)){
                // 表示在同一天，直接返回几小时前
                return (seconds / (60 *60L)) + "小时前";
            }else {
                // 表示不在同一天，返回昨天加小时+分钟
                return "昨天"+ creationTime.getHour() + ":" + creationTime.getMinute();
            }
        }
        // 4. 获取其他的昨天时间,即相同年月下的比较日期的判断是否相差一天, TODO 没有考虑一个月中的第一天和上一个月的最后一天
        if (creationTime.getYear() == now.getYear()&&
            creationTime.getMonthValue() ==now.getMonthValue()&&
            creationTime.getDayOfMonth() + 1 == now.getDayOfMonth()){
            return "昨天"+ creationTime.getHour() + ":" + creationTime.getMinute();
        }

        // 5. 同一年的时间 显示月份加日期 TODO 没有考虑一年的开头和结尾
        if (creationTime.getYear() == now.getYear()){
            return creationTime.getMonthValue()+"-" +creationTime.getDayOfMonth();
        }

        // 不在同一年
        return creationTime.getYear() + "-" + creationTime.getMonthValue() + "-" + creationTime.getDayOfMonth();
    }
}
