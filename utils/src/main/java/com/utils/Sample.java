package com.utils;


import com.aliyun.tea.*;
import constant.SmsConstant;

import java.util.Random;

public class Sample {

    /**
     * 使用AK&SK初始化账号Client
     * @return Client
     * @throws Exception 创建账号时抛出的异常
     */
    private static com.aliyun.dysmsapi20170525.Client createClient() throws Exception {
        com.aliyun.teaopenapi.models.Config config = new com.aliyun.teaopenapi.models.Config();
                config.setAccessKeyId(SmsConstant.accessKeyId);
                config.setAccessKeySecret(SmsConstant.AccessKeySecret);
        config.endpoint = SmsConstant.endpoint;
        return new com.aliyun.dysmsapi20170525.Client(config);
    }

    /**
     * 发送短信
     * @param phoneNumbers 电话号码
     * @param code         验证码，数字字符
     */
    public static void  sendSms(String phoneNumbers, String code) throws Exception {
        com.aliyun.dysmsapi20170525.Client client = Sample.createClient();
        com.aliyun.dysmsapi20170525.models.SendSmsRequest sendSmsRequest = new com.aliyun.dysmsapi20170525.models.SendSmsRequest()
                .setSignName(SmsConstant.signName)
                .setTemplateCode(SmsConstant.templateCode)
                .setPhoneNumbers(phoneNumbers)
                .setTemplateParam("{\"code\":\"" + code + "\"}");
        com.aliyun.teautil.models.RuntimeOptions runtime = new com.aliyun.teautil.models.RuntimeOptions();
        try {
            // 复制代码运行请自行打印 API 的返回值
            client.sendSmsWithOptions(sendSmsRequest, runtime);
        } catch (TeaException error) {
            // 此处仅做打印展示，请谨慎对待异常处理，在工程项目中切勿直接忽略异常。
            // 错误 message
            System.out.println(error.getMessage());
            // 诊断地址
            System.out.println(error.getData().get("Recommend"));
            com.aliyun.teautil.Common.assertAsString(error.message);
        } catch (Exception _error) {
            TeaException error = new TeaException(_error.getMessage(), _error);
            // 此处仅做打印展示，请谨慎对待异常处理，在工程项目中切勿直接忽略异常。
            // 错误 message
            System.out.println(error.getMessage());
            // 诊断地址
            System.out.println(error.getData().get("Recommend"));
            com.aliyun.teautil.Common.assertAsString(error.message);
        }
    }


    /**
     * 生成四位数的验证码
     * @return 验证码
     */
    public static String generateFourDigitVerificationCode() {
        Random random = new Random();
        int code = random.nextInt(9000) + 1000;
        return String.valueOf(code);
    }

    /**
     * 检查字符串是否是电话号码
     * @param input 电话号码
     * @return 布尔对象
     */
    public static boolean isPhoneNumber(String input) {
        // 检查输入是否为空或长度是否为11
        if (input == null || input.length() != 11) {
            return false;
        }

        // 检查输入是否只包含数字
        for (char c : input.toCharArray()) {
            if (!Character.isDigit(c)) {
                return false;
            }
        }
        // 检查改电话号码的第一个字符是不是 1 。如果是就返回true，不是就返回false
        return input.charAt(0) == '1';
    }
}
