package DTO;


import lombok.Data;

/**
 * 帖子排行榜分页查询
 */
@Data
public class RankingChatDTO {

    /* 需要请求的帖子数量 */
    private Integer chatNumber;

    /* 之前请求了多少个帖子，开头就以0开始 */
    private Integer number;

    /* 请求类型 1天，2周，3月 */
    private Integer type;
}
