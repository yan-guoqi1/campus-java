package DTO;

import lombok.Data;

/**
 * 用户发帖DTO
 */
@Data
public class PublishChatDTO {

    /* 标题 */
    private String title;

    /* 内容 */
    private String content;

    /* 标签id */
    private Integer signId;
}
