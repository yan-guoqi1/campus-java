package DTO;

import lombok.Data;

/**
 * 发布评论
 */
@Data
public class PubCommentDTO {

    /* 内容 */
    private String content;

    /* 帖子id */
    private Integer chatId;

    /* 是否是顶级评论 */
    private Integer isRoot;

    /* 顶级评论id */
    private Integer rootCommentsId;

    /* 目标评论id */
    private Integer goalId;
}
