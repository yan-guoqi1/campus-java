package DTO.MQ;

import lombok.Data;

@Data
public class DeleteCommentDO {
    private Integer chatId;

    private Integer number;
}
