package DTO;


import lombok.Data;

@Data
public class ReportChatDTO {

    /* 帖子id */
    private Integer chatId;

    /* 举报理由标签 */
    private Integer reportSign;

    /* 举报内容 */
    private String content;
}
