package DTO;


import lombok.Data;

/**
 * 修改密码
 */
@Data
public class EditPasswordDTO {

    /* 那种类型 0表示设置密码0 1表示修改密码 */
    private Integer type;

    /* 旧密码 */
    private String oldPassword;

    /* 新密码或者是用户初始设置的密码 */
    private String newPassword;

    /* 验证码，在设置密码的时候 */
    private String code;
}
