package DTO;

import lombok.Data;

/**
 * 帖子分页查询,获取用户的收藏帖子id
 */
@Data
public class ChatPageByCollectionDTO {
    /* 帖子请求数量 */
    private Integer chatNumber;

    /* 前面的数量 0 表示从开始查询*/
    private Integer number;

    /* 用户id */
    private Integer userId;
}
