package DTO;

import lombok.Data;

/**
 * 帖子分页查询
 */
@Data
public class ChatPageDTO {
    /* 帖子请求数量 */
    private Integer chatNumber;

    /* 上一个帖子id, 0表示获取最新请求 */
    private Integer lastChatId;

    /* 标签id 0表示获取全部帖子 */
    private Integer signId;
}
