package DTO;

import lombok.Data;

/**
 * 顶级评论的分页查询
 */
@Data
public class RootPageCommentDTO {
    /* 帖子id */
    private Integer chatId;

    /* 一次性取多少个数据 */
    private Integer number;

    /* 上一个评论id，如果是首次就设置为0 */
    private Integer lastCommentId;
}
