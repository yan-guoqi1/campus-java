package DTO;

import lombok.Data;

/**
 * 用户举报
 */
@Data
public class ReportCommentDTO {

    /* 举报内容*/
    private String content;

    /* 评论id */
    private Integer commentId;
}
