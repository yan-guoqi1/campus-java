package DTO;

import lombok.Data;

/**
 * 用户登录DTO
 */
@Data
public class UserLoginDTO {
    /* 登录方式,两种 0 表示验证码登录 1表示密码登录 */
    private Integer type;

    /* 用户手机号 */
    private String phoneNumber;

    /* 验证码 */
    private String code;

    /* 密码 */
    private String password;
}
