package DTO;

import lombok.Data;

/**
 * 帖子分页查询,获取用户的发帖id
 */
@Data
public class ChatPageByUserPubDTO {
    /* 帖子请求数量 */
    private Integer chatNumber;

    /* 上一个帖子id, 0表示获取最新请求 */
    private Integer lastChatId;

    /* 用户id */
    private Integer userId;
}
