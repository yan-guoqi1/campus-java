package BO;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 向前端返回图片信息
 */
@Data
public class ChatPictureBO {
    /* 顺序 */
    private Integer order;

    /* 图片地址，需要加密 */
    private String address;

    /* 图片宽度 */
    private Integer width;

    /* 图片高度 */
    private Integer height;
}
