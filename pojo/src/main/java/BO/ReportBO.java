package BO;

import lombok.Data;

@Data
public class ReportBO {
    /* 标签id */
    private Integer id;

    /* 内容 */
    private String reportSign;
}
