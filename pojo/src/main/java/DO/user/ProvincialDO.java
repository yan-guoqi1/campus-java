package DO.user;

import lombok.Data;

/**
 * 这个表只做查询操作
 */
@Data
public class ProvincialDO {

    /* 省id */
    private Integer pid;

    /* 省名称 */
    private String provincial;
}
