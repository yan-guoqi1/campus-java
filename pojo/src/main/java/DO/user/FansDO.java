package DO.user;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 粉丝表实体类
 */
@Data
public class FansDO {
    /* 主键id */
    private Integer id;

    /* 用户id */
    private Integer userId;

    /* 粉丝用户id */
    private Integer fansId;

    /* 创建时间 */
    private LocalDateTime createdTime;
}
