package DO.user;

import lombok.Data;

/**
 * 这个表只做查询操作
 */
@Data
public class CityDO {
    /* 城市id */
    private Integer cid;

    /* 城市名称 */
    private String city;

    /* 父级id */
    private Integer pid;
}
