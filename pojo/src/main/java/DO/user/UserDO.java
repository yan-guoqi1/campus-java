package DO.user;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 用户表实体类
 */
@Data
public class UserDO {
    /* 主键id */
    private Integer id;

    /* 手机号，账号 */
    private String phoneNumber;

    /* 用户姓名 */
    private String userName;

    /* 用户密码 */
    private String password;

    /* 盐 */
    private String salt;

    /* 用户头像地址 */
    private String avatar;

    /* 性别 1：男生 0：女生 */
    public Integer sex;

    /* 创建时间 */
    private LocalDateTime createdTime;

    /* 个人简介 */
    private String description;

    /* 出生年份 */
    private Integer year;

    /* 出生月份 */
    private Integer month;

    /* 出生日 */
    private Integer day;

    /* 一级地点 */
    private String firstPlace;

    /* 二级地点 */
    private String secondPlace;

    /* 爱好 */
    private String hobby;
}
