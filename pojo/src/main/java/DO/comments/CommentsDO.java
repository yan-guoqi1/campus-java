package DO.comments;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 评论表
 */
@Data
public class CommentsDO {
    /* 主键id */
    private Integer id;

    /* 帖子id */
    private Integer chatId;

    /* 发帖人id */
    private Integer chatUserId;

    /* 点赞数 */
    private Integer likeNumber;

    /* 评论人id */
    private Integer userId;

    /* 内容 */
    private String content;

    /* 创建时间 */
    private LocalDateTime createdTime;

     /* 是否是顶级评论 */
    private Integer isFirst;

    // 不是顶级评论之后，可以为空
    /* 顶级评论id */
    private Integer rootCommentsId;

    /* 回复那条评论的评论id */
    private Integer toCommentId;

    /* 回复那条评论的评论的发帖人id */
    private Integer toCommentUserId;
}
