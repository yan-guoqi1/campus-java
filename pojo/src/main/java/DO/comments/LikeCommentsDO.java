package DO.comments;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class LikeCommentsDO {

    /* 主键id */
    private Integer id;

    /* 评论id */
    private Integer commentId;

    /* 用户id */
    private Integer userId;

    /* 是否喜欢，点赞还是踩 1是喜欢 -1是踩 */
    private Integer isLike;

    /* 创建时间 */
    private LocalDateTime createdTime;
}
