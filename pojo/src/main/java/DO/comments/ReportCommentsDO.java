package DO.comments;

import lombok.Data;

/**
 * 帖子回复举报表
 */
@Data
public class ReportCommentsDO {

    /* 主键id */
    private Integer id;

    /* 用户id */
    private Integer userId;

    /* 举报原因 */
    private String reportReason;

    /* 评论id */
    private Integer commentId;

    /* 该评论的发布人id */
    private Integer subCommentId;

    /* 帖子id */
    private Integer chatId;

    /* 是否处理 */
    private Integer isHandle;
}
