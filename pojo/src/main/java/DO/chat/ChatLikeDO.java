package DO.chat;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 帖子点赞表
 */
@Data
public class ChatLikeDO {
    /* 主键id */
    private Integer id;

    /* 帖子id */
    private Integer chatId;

    /* 用户id，点赞人id */
    private Integer userId;

    /* 创建时间 */
    private LocalDateTime createdTime;
}
