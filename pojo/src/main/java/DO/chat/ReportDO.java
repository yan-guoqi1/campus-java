package DO.chat;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 帖子举报表
 */
@Data
public class ReportDO {
    /* 主键id */
    private Integer id;

    /* 帖子id */
    private Integer chatId;

    /* 用户id */
    private Integer userId;

    /* 举报原因 */
    private String reportReason;

    /* 是否处理，默认为0 表示未处理，1表示已经处理 */
    private Integer isHandle;

    /* 创建时间 */
    private LocalDateTime createdTime;

    /* 举报标签 */
    private Integer reportSign;

    /* 图片地址1 */
    private String picture1;

    /* 图片地址1 */
    private String picture2;

    /* 图片地址1 */
    private String picture3;

    /* 图片地址1 */
    private String picture4;

}
