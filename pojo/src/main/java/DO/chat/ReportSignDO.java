package DO.chat;


import lombok.Data;


/**
 * 帖子标签举报表
 */
@Data
public class ReportSignDO {

    /* 主键id */
    private Integer id;

    /* 标签内容 */
    private String reportSign;

    /* 标签级别 */
    private Integer level;

    /* 父级标签 */
    private Integer fatherId;
}
