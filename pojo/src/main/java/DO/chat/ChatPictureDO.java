package DO.chat;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 帖子图片关系表
 */
@Data
public class ChatPictureDO {
    /* 主键id */
    private Integer id;

    /* 帖子id */
    private Integer chatId;

    /* 顺序 */
    private Integer order;

    /* 图片地址 */
    private String address;

    /* 图片宽度 */
    private Integer width;

    /* 图片高度 */
    private Integer height;

    /* 创建时间 */
    private LocalDateTime createdTime;
}
