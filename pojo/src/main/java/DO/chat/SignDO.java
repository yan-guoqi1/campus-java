package DO.chat;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 标签表
 */
@Data
public class SignDO {
    /* 主键id */
    private Integer id;

    /* 标签 */
    private String sign;

    /* 创建时间 */
    private LocalDateTime createdTime;
}
