package DO.chat;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 帖子收藏表
 */
@Data
public class ChatCollectionDO {
    /* 主键id */
    private Integer id;

    /* 帖子id */
    private Integer chatId;

    /* 用户id，收藏家id */
    private Integer userId;

    /* 创建时间 */
    private LocalDateTime createdTime;
}
