package DO.chat;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 帖子表实体类
 */
@Data
public class ChatDO {
    /* 主键id */
    private Integer id;

    /* 用户id，发贴人 */
    private Integer userId;

    /* 标题 */
    private String title;

    /* 内容,可以为空 */
    private String content;

    /* 照片数量 */
    private Integer pictureNumber;

    /* 创建时间 */
    private LocalDateTime createdTime;

    /* 点赞数 */
    private Integer likeNumber;

    /* 收藏数 */
    private Integer collectionNumber;

    /* 阅读量 Long 类型 */
    private Long readNumber;

    /* 标签id */
    private Integer signId;

    /* 评论数 */
    private Integer commentsNumber;
}
