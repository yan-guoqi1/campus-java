package DO.chat;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 敏感词表
 */
@Data
public class SensitiveDO {
    /* 主键id */
    private Integer id;

    /* 敏感词 */
    private String sensitive;

    /* 创建时间 */
    private LocalDateTime createdTime;
}
