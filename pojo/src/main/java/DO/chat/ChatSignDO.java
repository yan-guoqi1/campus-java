package DO.chat;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 帖子标签表
 */
@Data
public class ChatSignDO {
    /* 主键id */
    private Integer id;

    /* 帖子id */
    private Integer chatId;

    /* 标签id */
    private Integer signId;

    /* 创建时间 */
    private LocalDateTime createdTime;
}
