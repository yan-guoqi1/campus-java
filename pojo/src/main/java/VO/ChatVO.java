package VO;

import BO.ChatPictureBO;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class ChatVO {

    /* 帖子id */
    private Integer chatId;

    /* 帖子标题 */
    private String title;

    /* 帖子内容 */
    private String content;

    /* 图片地址信息 数组，每一个请求地址都是图片返回 长度是数量 */
    private List<ChatPictureBO> chatPictureBOList;

    /* 点赞数 */
    private Integer likeNumber;

    /* 收藏数 */
    private Integer collectionNumber;

    /* 回复数,评论 */
    private Integer commentsNumber;

    /* 点击量 */
    private Long readNumber;

    /* 用户id，发帖人id */
    private Integer userId;

    /* 用户昵称 */
    private String username;

    /* 关注信息 已关注 1，未关注 0 */
    private Integer follow;

    /* createdTime */
    private LocalDateTime createdTime;

    /* 根据创建时间 createdTime分别显示几分钟前，几小时前，几天前 */
    private String time;

    /* 标签 */
    private String sign;

    /* 是否收藏  0 表示没有 1表示收藏*/
    private Integer collection;

    /* 是否点赞 0 表示没有 1表示有 */
    private Integer like;
}
