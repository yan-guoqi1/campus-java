package VO;

import lombok.Data;

/**
 * 前端用于展示标签，不带时间，时间用来排序
 */
@Data
public class SignVO {
    private String sign;

    private Integer id;
}
