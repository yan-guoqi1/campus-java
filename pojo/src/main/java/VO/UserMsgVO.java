package VO;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class UserMsgVO {
    /* 用户id */
    private Integer id;

    /* 电话号码 */
    private String phoneNumber;

    /* 用户账号 */
    private String userName;

    /* 性别 1表示男 0表示女 */
    private Integer sex;

    /* 关注数 */
    private Integer followNumber;

    /* 粉丝数 */
    private Integer fensNumber;

    /* 作品数量 */
    private Integer chatNumber;

    /* 个人简介 */
    private String description;

    /* 出生年份 */
    private Integer year;

    /* 出生月份 */
    private Integer month;

    /* 出生日 */
    private Integer day;

    /* 一级地点 */
    private String firstPlace;

    /* 二级地点 */
    private String secondPlace;

    /* 创建时间 */
    private LocalDateTime createdTime;

    /* 年龄 */
    private Integer age;

    /* 爱好 */
    private String hobby;

    /* 点赞数 */
    private Integer chatLikeNumber;
}
