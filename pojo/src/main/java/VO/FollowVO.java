package VO;

import lombok.Data;


/**
 * 用户关注数据
 */
@Data
public class FollowVO {

    /* 关注者id */
    private Integer followId;

    /* 关注者昵称 */
    private String userName;

    /* 关注头像地址 */
    private String avatar;

    /* 关注者简介 */
    private String description;

    /* 与请求者的关注关系。1：请求者已经关注 0 请求者未关注(包含粉丝已经关注或者没有关注请求者) -1 请求者与该粉丝是互关，朋友关系 */
    private Integer follow;
}
