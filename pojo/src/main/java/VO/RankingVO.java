package VO;

import lombok.Data;

/**
 * 帖子排行榜结果返回类
 */
@Data
public class RankingVO {

    /* 帖子id */
    private Integer id;

    /* 帖子标题 */
    private String title;

    /* 帖子热度值 */
    private Double score;
}
