package VO;

import lombok.Data;


/**
 * 用户粉丝数据
 */
@Data
public class FansVO {

    /* 粉丝id */
    private Integer fansId;

    /* 粉丝昵称 */
    private String userName;

    /* 粉丝头像地址 */
    private String avatar;

    /* 粉丝简介 */
    private String description;

    /* 与请求者的关注关系。1：请求者已经关注 0 请求者未关注(包含粉丝已经关注或者没有关注请求者) -1 请求者与该粉丝是互关，朋友关系 */
    private Integer follow;
}
