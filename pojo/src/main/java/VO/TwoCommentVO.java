package VO;

import lombok.Data;

@Data
public class TwoCommentVO {

    /* 评论id */
    private Integer id;

    /* 评论人id 根据这个id获取这个人的昵称和头像*/
    private Integer userId;

    /* 用户昵称 */
    private String username;

    /* 头像昵称 */
    private String avatar;

    /* 点赞数 */
    private Integer likeNumber;

    /* 内容 */
    private String content;

    /* 创建时间 修改为几天前这种*/
    private String time;

    /* 用户是否点赞 1是 0否 -1表示踩，不显示这个评论 */
    private Integer isLike;

    /* 顶级评论id */
    private Integer rootCommentId;

    /* 目标评论id */
    private Integer toCommentId;

    /* 目标评论的发帖人id */
    private Integer toCommentUserId;

    /* 目标评论的发帖人名称 */
    private String toCommentUsername;
}
