package VO.Feight;


import lombok.Data;

@Data
public class UserMsgByChatVO {

    /* 作品数 */
    private Integer chatNumber;

    /* 或赞数 */
    private Integer chatLikeNumber;
}
