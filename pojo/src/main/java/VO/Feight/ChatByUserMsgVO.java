package VO.Feight;

import lombok.Data;

/**
 * 获取帖子信息需要的用户信息
 */
@Data
public class ChatByUserMsgVO {

    private String username;

    private Integer follow;
}
