package VO.Feight;

import lombok.Data;

/**
 * 展示评论人的头像以及昵称消息
 */
@Data
public class CommentUserMsgVO {
    /* 用户昵称 */
    private String username;

    /* 用户头像 */
    private String avatar;
}
