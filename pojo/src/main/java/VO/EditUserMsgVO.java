package VO;

import lombok.Data;

@Data
public class EditUserMsgVO {
    /* 用户昵称 */
    private String userName;

    /* 用户性别 1表示男 0表示女 */
    private Integer sex;

    /* 个人简介 */
    private String description;

    /* 出生年份 */
    private Integer year;

    /* 出生月份 */
    private Integer month;

    /* 出生日 */
    private Integer day;

    /* 一级地点 */
    private String firstPlace;

    /* 二级地点 */
    private String secondPlace;

    /* 爱好 */
    private String hobby;
}
