package VO;

import BO.ReportBO;
import lombok.Data;

import java.util.List;

/**
 * 返回给前端的举报标签类
 */
@Data
public class ReportSignVO {

    private List<ReportBO> first;

    /* 按照一级标签id顺序进行排序 */
    private List<List<ReportBO>> second;
}
