package com.chat;

import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.Map;

@SpringBootTest
public class SpringAmqpTest {


    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test
    void testSend() {
        String exchangeName = "ying.yan";
        //String msg = "今天天气挺不错，我的心情的挺好的";
        Map<String,Object> map = new HashMap<>();
        map.put("name","jack");
        map.put("age",10);
        rabbitTemplate.convertAndSend(exchangeName,"chat.save.comments", map);
    }

}
