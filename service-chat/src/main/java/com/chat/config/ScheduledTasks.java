package com.chat.config;

import constant.RedisPreKeyConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Set;

import static com.utils.RankingUtils.secondsRemaining;

@Component
@Slf4j
public class ScheduledTasks {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Scheduled(fixedRate = 1000*60*60*24,initialDelay = 5000) // 每一天执行一次
    public void dayCurrentTime() {
        log.info("检查day的排行榜是否有帖子时间超过1天");
        Set<ZSetOperations.TypedTuple<Object>> set1 = redisTemplate.opsForZSet().reverseRangeWithScores(RedisPreKeyConstant.RANKING_DAY, 0L, -1L);
        if (set1 != null) {
            for (ZSetOperations.TypedTuple<Object> tuple : set1) {
                String[] parts = Objects.requireNonNull(tuple.getValue()).toString().split("_"); // 使用 "_" 作为分隔符拆分字符串

                long longPart = Long.parseLong(parts[0]); // 将第一部分转换为 long, 秒数
                if (!secondsRemaining(longPart,1)){
                    // 表示已经过期了,删除redis中的这个值
                    redisTemplate.opsForZSet().remove(RedisPreKeyConstant.RANKING_DAY, tuple.getValue());
                }
            }
        }
    }

    @Scheduled(fixedRate = 1000*60*60*24*7,initialDelay = 5000) // 每7天执行一次
    public void weekCurrentTime() {
        log.info("检查week的排行榜是否有帖子时间超过7天");
        Set<ZSetOperations.TypedTuple<Object>> set = redisTemplate.opsForZSet().reverseRangeWithScores(RedisPreKeyConstant.RANKING_WEEK, 0L, -1L);
        if (set != null) {
            for (ZSetOperations.TypedTuple<Object> tuple : set) {
                String[] parts = Objects.requireNonNull(tuple.getValue()).toString().split("_"); // 使用 "_" 作为分隔符拆分字符串

                long longPart = Long.parseLong(parts[0]); // 将第一部分转换为 long, 秒数
                if (!secondsRemaining(longPart,7)){
                    // 表示已经过期了,删除redis中的这个值
                    redisTemplate.opsForZSet().remove(RedisPreKeyConstant.RANKING_WEEK, tuple.getValue());
                }
            }
        }
    }

    @Scheduled(fixedRate = 1000 * 60 * 60 * 24 * 30L,initialDelay = 5000) // 每30天执行一次
    public void monthCurrentTime() {
        log.info("检查month的排行榜是否有帖子时间超过30天");
        Set<ZSetOperations.TypedTuple<Object>> set = redisTemplate.opsForZSet().reverseRangeWithScores(RedisPreKeyConstant.RANKING_MONTH, 0L, -1L);
        if (set != null) {
            for (ZSetOperations.TypedTuple<Object> tuple : set) {
                String[] parts = Objects.requireNonNull(tuple.getValue()).toString().split("_"); // 使用 "_" 作为分隔符拆分字符串

                long longPart = Long.parseLong(parts[0]); // 将第一部分转换为 long, 秒数
                if (!secondsRemaining(longPart,30)){
                    // 表示已经过期了,删除redis中的这个值
                    redisTemplate.opsForZSet().remove(RedisPreKeyConstant.RANKING_MONTH, tuple.getValue());
                }
            }
        }
    }

}