package com.chat.config;


import Exception.*;
import constant.CodeConstant;
import constant.MsgConstant;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import result.Result;

/**
 * 全局异常处理器
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    // 最初异常,code = 500,msg="网络错误“
    @ExceptionHandler(BaseException.class)
    @ResponseBody
    public Result<String> handleException(Exception ex) {
        return Result.error(MsgConstant.NETWORK_ERROR, CodeConstant.NETWORK_ERROR);
    }


    // 图片格式不正常
    @ExceptionHandler(ImageIsNotRightException.class)
    @ResponseBody
    public Result<String> ImageIsNotRightException(ImageIsNotRightException ex){
        return Result.error(MsgConstant.IMAGE_FORMAT_NOT_RIGHT,CodeConstant.IMAGE_FORMAT_NOT_RIGHT);
    }


    // 帖子不存在
    @ExceptionHandler(ChatIsNotExistException.class)
    @ResponseBody
    public Result<String> ChatIsExistException(ChatIsNotExistException ex){
        return Result.error(MsgConstant.CHAT_IS_EXIST,CodeConstant.CHAT_IS_EXIST);
    }


    // 用户不存在
    @ExceptionHandler(UserNotExistException.class)
    @ResponseBody
    public Result<String> UserNotExistException(UserNotExistException ex){
        return Result.error(MsgConstant.USER_IS_NOT_EXIST, CodeConstant.USER_NOT_EXIST);
    }

    // 重复收藏
    @ExceptionHandler(ExistCollectionChatException.class)
    @ResponseBody
    public Result<String> ExistCollectionChatException(ExistCollectionChatException ex){
        return Result.error(MsgConstant.REPEAT_COLLECTION,CodeConstant.REPEAT_COLLECTION);
    }

    // 重复点赞
    @ExceptionHandler(ExistLikeChatException.class)
    @ResponseBody
    public Result<String> ExistLikeChatException(ExistLikeChatException ex){
        return Result.error(MsgConstant.REPEAT_LIKE,CodeConstant.REPEAT_LIKE);
    }

    // 没有收藏关系
    @ExceptionHandler(NotCollectionChatException.class)
    @ResponseBody
    public Result<String> NotCollectionChatException(NotCollectionChatException ex){
        return Result.error(MsgConstant.NOT_COLLECTION,CodeConstant.NOT_COLLECTION);
    }

    // 没有点赞关系
    @ExceptionHandler(NotLikeChatException.class)
    @ResponseBody
    public Result<String> NotLikeChatException(NotLikeChatException ex){
        return Result.error(MsgConstant.NOT_LIKE,CodeConstant.NOT_LIKE);
    }
}
