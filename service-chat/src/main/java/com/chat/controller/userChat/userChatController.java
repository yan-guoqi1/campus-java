package com.chat.controller.userChat;

import DTO.ChatPageByCollectionDTO;
import DTO.ChatPageByUserPubDTO;
import com.chat.service.ChatService;
import constant.CodeConstant;
import constant.MsgConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import result.Result;

import java.util.List;


/**
 * 用户帖子信息管理，需要用户权限控制，用户设置自己的这些帖子信息是否可见
 */
@RestController
@RequestMapping("/campus/chat")
public class userChatController {

    @Autowired
    private ChatService chatService;

    /**
     * 获取用户的发帖id数组
     * @return
     * 103 参数不完整
     */
    @PostMapping("/getUserPubChat")
    public Result<List<Integer>> getUserPubChat(@RequestHeader(name = "userId",required = false) String userId,
            @RequestBody ChatPageByUserPubDTO chatPageByUserPubDTO){

        // 1. 判断参数是否完整
        if (chatPageByUserPubDTO.getChatNumber() == null||chatPageByUserPubDTO.getChatNumber()<1||
            chatPageByUserPubDTO.getLastChatId() == null||chatPageByUserPubDTO.getLastChatId()<0||
            chatPageByUserPubDTO.getUserId()<1){
            // 参数不完整
            return Result.error(MsgConstant.PARAM_NOT_SATISFY, CodeConstant.PARAM_NOT_SATISFY);
        }

        // 2. 判断用户信息
        if(!Integer.valueOf(userId==null? "0" :userId).equals(chatPageByUserPubDTO.getUserId())){
            // 2.1 表示访问别人的数据 TODO 发feign判断用户是否存在和获取是否其他用户访问自己的发帖信息
        }

        return Result.success(chatService.getUserPubChatId(chatPageByUserPubDTO));
    }

    /**
     * 获取用户的收藏帖子id
     * @param userId
     * @param chatPageByCollectionDTO
     * @return
     */
    @PostMapping("/getUserChatIdByCollection")
    public Result<List<Integer>> getUserChatIdByCollection(@RequestHeader(name = "userId",required = false) String userId,
                                                           @RequestBody ChatPageByCollectionDTO chatPageByCollectionDTO){
        // 1. 判断参数是否完整
        if (chatPageByCollectionDTO.getChatNumber() == null||chatPageByCollectionDTO.getChatNumber()<1||
                chatPageByCollectionDTO.getNumber() == null||chatPageByCollectionDTO.getNumber()<0||
                chatPageByCollectionDTO.getUserId()<1){
            // 参数不完整
            return Result.error(MsgConstant.PARAM_NOT_SATISFY, CodeConstant.PARAM_NOT_SATISFY);
        }

        // 2. 判断用户信息
        if(!Integer.valueOf(userId==null? "0" :userId).equals(chatPageByCollectionDTO.getUserId())){
            // 2.1 表示访问别人的数据 TODO 发feign判断用户是否存在和获取是否允许用户访问自己的收藏的帖子
        }

        return Result.success(chatService.getUserChatIdByCollection(chatPageByCollectionDTO));
    }
}
