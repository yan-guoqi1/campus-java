package com.chat.controller.Feign;


import VO.Feight.UserMsgByChatVO;
import com.chat.service.ChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import result.Result;

@RestController
@RequestMapping("/feign/chat")
public class FeignChatController {

    @Autowired
    private ChatService chatService;

    @GetMapping("/getChatNumberByUserId/{userId}")
    public UserMsgByChatVO getChatNumberByUserId(@PathVariable Integer userId){
        return chatService.getChatNumberByUserId(userId);
    }
}
