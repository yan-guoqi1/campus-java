package com.chat.controller.campus;

import DTO.ChatPageDTO;
import DTO.RankingChatDTO;
import VO.ChatVO;
import VO.RankingVO;
import VO.ReportSignVO;
import com.chat.service.ChatService;
import constant.CodeConstant;
import constant.MsgConstant;
import org.aspectj.apache.bcel.classfile.Code;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import result.Result;

import java.util.List;

/**
 * 帖子
 */
@RestController
@RequestMapping("/campus/chat")
public class CampusChatController {

    @Autowired
    private ChatService chatService;

    /**
     * 帖子分页查询id
     * @param chatPageDTO 请求信息
     * @return 帖子id数据
     * 103 参数不满足要求
     */
    @PostMapping("/getChatPage")
    public Result<List<Integer>> getChatPage(@RequestBody ChatPageDTO chatPageDTO){
        // 1. 对参数进行判断 lastChatId = 0获取最新帖子 signId=0表示获取全部标签
        if (chatPageDTO.getLastChatId()<0||
            chatPageDTO.getChatNumber()<1||
            chatPageDTO.getSignId()<0
            ){
            return Result.error(MsgConstant.PARAM_NOT_SATISFY, CodeConstant.PARAM_NOT_SATISFY);
        }
        return Result.success(chatService.getChatIdArray(chatPageDTO));
    }


    /**
     * 根据帖子id获取具体的信息
     * @return userId===0 表示用户未登录
     * 301 帖子不存在
     */
    @GetMapping("/getChatById")
    public Result<ChatVO> getChatById(@RequestParam("chatId") Integer chatId,
                                      @RequestParam(name = "userId") Integer userId){
        userId=(userId==null?0:userId);
        if (chatId<=0||userId<0){
            return Result.error(MsgConstant.PARAM_NOT_SATISFY,CodeConstant.PARAM_NOT_SATISFY);
        }
        // 如果userId为null，将0作为userId表示这个id不存在
        return Result.success(chatService.getChatById(chatId,userId));
    }


    /**
     * 获取举报标签
     * @return 标签数据
     */
    @GetMapping("/getReportSign")
    public Result<ReportSignVO> getReportSign(){
        return Result.success(chatService.getReportSign());
    }


    /**
     * 获取排行榜数据
     * @param rankingChatDTO
     * @return
     */
    @PostMapping("/getRanking")
    public Result<List<RankingVO>> getRanking(@RequestBody RankingChatDTO rankingChatDTO){

        if (rankingChatDTO.getChatNumber()<1||rankingChatDTO.getNumber()<0||rankingChatDTO.getType()<1||rankingChatDTO.getType()>3){
            return Result.error(MsgConstant.PARAM_NOT_SATISFY, CodeConstant.PARAM_NOT_SATISFY);
        }

        return Result.success(chatService.getRanking(rankingChatDTO));
    }

}
