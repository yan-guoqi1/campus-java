package com.chat.controller.campus;


import VO.SignVO;
import com.chat.service.SignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import result.Result;

import java.util.List;

@RestController
@RequestMapping("/campus/chat/sign")
public class SignController {

    @Autowired
    private SignService signService;


    /**
     * 获取标签
     * @return
     */
    @GetMapping("getSign")
    public Result<List<SignVO>> getSign(){
        return Result.success(signService.getSign());
    }
}
