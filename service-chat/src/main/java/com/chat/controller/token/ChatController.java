package com.chat.controller.token;

import DTO.PubCommentDTO;
import DTO.PublishChatDTO;
import DTO.ReportChatDTO;
import VO.ReportSignVO;
import com.chat.service.ChatService;
import com.chat.service.SignService;
import com.feign.clients.UserClient;
import constant.CodeConstant;
import constant.MsgConstant;
import org.apache.ibatis.annotations.Delete;
import org.aspectj.apache.bcel.classfile.Code;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import result.Result;

import java.util.List;

@RestController
@RequestMapping("/chat")
public class ChatController {

    @Autowired
    private UserClient userClient;

    @Autowired
    private SignService signService;

    @Autowired
    private ChatService chatService;

    /**
     * 用户发帖
     * @param userId 发帖人id
     * @param title 标题
     * @param content 内容
     * @param signId 标签id
     * @param files 图片文件 可以不选
     * @return
     * 14 用户不存在，当我们后台删除这个用户的时候而此时用户正在发帖（少见，但不能舍弃）
     * 104 标签列表为空
     * 51 图片格式不正确
     * 101 缺少必要的参数（参数为空）
     * 标签是否存在 不存在 或者解析文件时报错 500 网络错误
     */
    @PostMapping("/publishChat")
    public Result<String> publishChat(@RequestHeader(name = "userId") String userId,
                                      @RequestParam("title") String title,
                                      @RequestParam("content") String content,
                                      @RequestParam("signId") Integer signId,
                                      @RequestParam(name = "files", required = false) MultipartFile[] files) {
        // 1. 判断参数是否完整
        if(signId==null|| title==null|| content==null){
            return Result.error(MsgConstant.LACK_PARAM,CodeConstant.LACK_PARAM);
        }

        // 2. 向user-service发请求，判断发帖人是否存在
        if (!userClient.findById(Integer.valueOf(userId))){
            // 如果不存在，则抛出异常
            return Result.error(MsgConstant.USER_IS_NOT_EXIST, CodeConstant.USER_NOT_EXIST);
        }

        // 2. 判断标签是否存在 不存在 500 网络错误
        signService.judgeSignIsExist(signId);

        // 3. 交给service进行帖子以及图片文件的保存
        PublishChatDTO publishChatDTO = new PublishChatDTO();
        publishChatDTO.setContent(content);
        publishChatDTO.setTitle(title);
        publishChatDTO.setSignId(signId);
        chatService.publishChat(userId,publishChatDTO,files);

        return Result.success();
    }


    /**
     * 用户收藏帖子
     * 301 帖子不存在
     * 302 重复收藏
     * 14 用户不存在
     */
    @GetMapping("/collection")
    public Result<String> collection(@RequestHeader(name = "userId") String userId,
                                     @RequestParam("chatId") Integer chatId){
        chatService.collectionById(userId,chatId);
        return Result.success();
    }


    /**
     * 用户取消收藏
     * @param userId 用户id
     * @param chatId 帖子id
     * @return
     * 301 帖子不存在
     * 304 没有收藏关系
     * 14 用户不存在
     */
    @DeleteMapping("/deleteCollection")
    public Result<String> deleteCollection(@RequestHeader(name = "userId") String userId,
                                           @RequestParam("chatId") Integer chatId){
        chatService.deleteCollectionById(Integer.valueOf(userId),chatId);
        return Result.success();
    }


    /**
     * 用户点赞帖子
     * @param userId 用户id
     * @param chatId 帖子id
     * @return 成功消息
     * 301 帖子不存在
     * 303 重复点赞
     * 14 用户不存在
     */
    @GetMapping("/like")
    public Result<String> like(@RequestHeader(name = "userId") String userId,
                               @RequestParam("chatId") Integer chatId){
        chatService.like(Integer.valueOf(userId),chatId);
        return Result.success();
    }


    /**
     * 用户取消点赞
     * @param userId 用户id
     * @param chatId 帖子id
     * @return 成功消息
     * 301 帖子不存在
     * 305 没有点赞关系
     * 14 用户不存在
     */
    @DeleteMapping("/deleteLike")
    public Result<String> deleteLike(@RequestHeader(name = "userId") String userId,
                                     @RequestParam("chatId") Integer chatId){
        chatService.deleteLike(Integer.valueOf(userId),chatId);
        return Result.success();
    }


    /**
     * 发布评论
     * @param userId 用户id
     * @param pubCommentDTO 评论
     * @return
     * 103 参数不符合要求
     * 101 缺少必要的参数
     */
    @PutMapping("/pubChatComment")
    public Result<String> pubChatComment(@RequestHeader(name = "userId") String userId,
                                         @RequestBody PubCommentDTO pubCommentDTO){
        if (!(pubCommentDTO.getIsRoot() == 0 || pubCommentDTO.getIsRoot() == 1)){
            return Result.error(MsgConstant.PARAM_NOT_SATISFY,CodeConstant.PARAM_NOT_SATISFY);
        }
        if (pubCommentDTO.getIsRoot() != 1){
            // 表示不是顶级评论,则目标评论这些参数不能为空
            if (pubCommentDTO.getRootCommentsId() == null||pubCommentDTO.getGoalId()==null){
                return Result.error(MsgConstant.LACK_PARAM, CodeConstant.LACK_PARAM);
            }
        }
        chatService.pubChatComment(pubCommentDTO,Integer.valueOf(userId));
        return Result.success();
    }

    /**
     * 用户删除帖子
     * @param userId 用户id
     * @param chatId 帖子id
     * @return 成功标识
     */
    @DeleteMapping("/delete")
    public Result<String> deleteChat(@RequestHeader(name = "userId") String userId,
                                     @RequestParam Integer chatId){

        if (userId==null||chatId==null||chatId<1){
            return Result.error(MsgConstant.PARAM_NOT_SATISFY,CodeConstant.PARAM_NOT_SATISFY);
        }
        chatService.deleteChat(Integer.valueOf(userId),chatId);
        return Result.success();
    }


    /**
     * 用户举报帖子
     * @param userId 用户id
     * @return 返回成功消息 301 帖子不存在或者已经删除
     */
    @PostMapping("/reportChat")
    public Result<String> reportChat(@RequestHeader(name = "userId") String userId,
                                     @RequestParam("chatId") Integer chatId,
                                     @RequestParam("reportSign") Integer reportSign,
                                     @RequestParam("content") String content,
                                     @RequestParam(name = "files", required = false) MultipartFile[] files){
        // 1. 参数处理
        if (chatId<1||reportSign<1||content.length()>200||files.length>4){
            return Result.error(MsgConstant.PARAM_NOT_SATISFY,CodeConstant.PARAM_NOT_SATISFY);
        }
        ReportChatDTO reportChatDTO = new ReportChatDTO();
        reportChatDTO.setChatId(chatId);
        reportChatDTO.setReportSign(reportSign);
        reportChatDTO.setContent(content);

        chatService.reportChat(reportChatDTO,userId,files);
        return Result.success();
    }


}
