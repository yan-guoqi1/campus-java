package com.chat.mapper;

import DTO.ChatPageByCollectionDTO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDateTime;
import java.util.List;

@Mapper
public interface CollectionNumberMapper {

    // 用户收藏
    @Insert("insert into tb_chat_collection (chat_id, user_id, created_time) VALUES (#{chatId},#{userId},#{now})")
    void save(Integer chatId, Integer userId, LocalDateTime now);

    // 判断用户对这个有无收藏关系
    @Select("select COUNT(*) from tb_chat_collection where chat_id = #{chatId} and user_id = #{userId}")
    int getByChatIdAndUserId(Integer chatId, Integer userId);

    // 删除用户的收藏关系
    @Delete("delete from tb_chat_collection where chat_id = #{chatId} and user_id = #{userId}")
    void delete(Integer chatId, Integer userId);

    // 分页查询用户的收藏信息
    @Select("SELECT chat_id FROM tb_chat_collection where user_id = #{userId} LIMIT #{chatNumber} OFFSET #{number}")
    List<Integer> getUserChatIdByCollection(ChatPageByCollectionDTO chatPageByCollectionDTO);

    // 删除帖子相关的收藏数据
    @Delete("delete from tb_chat_collection where chat_id = #{chatOId}")
    void deleteByChatId(Integer chatId);
}
