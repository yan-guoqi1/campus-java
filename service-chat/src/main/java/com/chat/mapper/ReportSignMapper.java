package com.chat.mapper;

import BO.ReportBO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ReportSignMapper {

    @Select("select id, report_sign from report_sign where level = 1 ORDER BY id")
    List<ReportBO> getFirst();

    @Select("select id, report_sign from report_sign where level = 2 and father_id = #{id} ORDER BY id")
    List<ReportBO> getSecond(Integer id);
}
