package com.chat.mapper;

import DO.chat.ChatPictureDO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ChatPictureMapper {
    // 保存帖子的照片信息
    void save(List<ChatPictureDO> chatPictureDOList);

    // 根据id获取图片的全部数据
    @Select("select * from tb_chat_picture where chat_id = #{chatId}")
    List<ChatPictureDO> getByChatId(Integer chatId);

    // 删除帖子的图片信息
    @Delete("delete from tb_chat_picture where chat_id = #{chatId}")
    void deleteByChat(Integer chatId);
}
