package com.chat.mapper;

import DO.chat.ChatSignDO;
import VO.SignVO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SignMapper {


    // 获取标签信息
    @Select("select id, sign from tb_sign")
    List<SignVO> getSign();

    // 获取全部标签id
    @Select("select id from tb_sign")
    Integer[] getSignId();

    // 根据标签id获取具体的标签
    @Select("select sign from tb_sign where id = #{signId}")
    String getSignById(Integer signId);

    @Delete("delete from tb_chat_sign where chat_id = #{chatId}")
    void deleteByChatId(Integer chatId);
}
