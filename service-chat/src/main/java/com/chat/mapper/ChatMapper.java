package com.chat.mapper;

import DO.chat.ChatDO;
import DTO.ChatPageByUserPubDTO;
import DTO.ChatPageDTO;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ChatMapper {

    // 保存帖子主要信息
    void save(ChatDO chatDO);

    @Select("select COUNT(*) from tb_chat where user_id = #{userId}")
    Integer getChatNumberByUserId(Integer userId);

    // 获取帖子id
    @Select("SELECT id FROM tb_chat where (#{lastChatId} = 0 OR id < #{lastChatId}) " +
            "and (#{signId} = 0 OR sign_id = #{signId}) ORDER BY created_time DESC\n" +
            "LIMIT #{chatNumber}")
    List<Integer> getChatIdArray(ChatPageDTO chatPageDTO);

    // 根据id获取帖子信息
    @Select("select * from tb_chat where id = #{chatId}")
    ChatDO getById(Integer chatId);

    // 新增收藏
    @Update("update tb_chat set collection_number = collection_number + 1 where id = #{chatId}")
    void updateChatCollection(Integer chatId);

    // 新增点赞
    @Update("update tb_chat set like_number = like_number + 1 where id = #{chatId}")
    void updateChatLike(Integer chatId);

    // 删除一个收藏记录
    @Update("update tb_chat set collection_number = collection_number - 1 where id = #{chatId}")
    void DeleteChatCollection(Integer chatId);

    // 删除一个点赞消息
    @Update("update tb_chat set like_number = like_number - 1 where id = #{chatId}")
    void deleteChatLike(Integer chatId);

    // 新增评论
    @Update("update tb_chat set comments_number = comments_number + 1 where id = #{chatId}")
    void updateChatComments(Integer chatId);


    // 获取用户发布的帖子id
    @Select("SELECT id FROM tb_chat where (#{lastChatId} = 0 OR id < #{lastChatId}) " +
            "and (user_id = #{userId}) ORDER BY created_time DESC\n" +
            "LIMIT #{chatNumber}")
    List<Integer> getUserPubChatId(ChatPageByUserPubDTO chatPageByUserPubDTO);


    // 获取点赞数
    @Select("select like_number from tb_chat where user_id = #{userId}")
    List<Integer> getLikeNumberByUserId(Integer userId);

    // 删除帖子
    @Delete("delete from tb_chat where id = #{chatId}")
    void deleteChatById(Integer chatId);


    // 获取一天内的帖子数据
    @Select("select * from tb_chat where created_time >= NOW() - INTERVAL 1 DAY")
    List<ChatDO> getChatByDay();

    // 获取一周内的帖子数据
    @Select("select * from tb_chat where created_time >= NOW() - INTERVAL 7 DAY")
    List<ChatDO> getChatByWeek();

    // 获取一个月内的帖子数据
    @Select("select * from tb_chat where created_time >= NOW() - INTERVAL 30 DAY")
    List<ChatDO> getChatByMonth();

    // 删除评论
    @Update("update tb_chat set comments_number = comments_number - #{number} where id = #{chatId}")
    void deleteComment(Integer chatId, Integer number);
}
