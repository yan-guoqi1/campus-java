package com.chat.mapper;

import DO.chat.ReportDO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface ReportMapper {

    // 保存用户举报数据
    @Insert("insert into tb_report (chat_id, user_id, report_reason, created_time, report_sign, picture1, picture2, picture3, picture4) VALUES " +
            "(#{chatId},#{userId},#{reportReason},#{createdTime},#{reportSign},#{picture1},#{picture2},#{picture3},#{picture4})")
    void save(ReportDO reportDO);
}
