package com.chat.mapper;

import org.apache.ibatis.annotations.*;

import java.time.LocalDateTime;

@Mapper
public interface LikeMapper {

    // 判断是否存在关注消息
    @Select("select COUNT(*) from tb_chat_like where user_id = #{userId} and chat_id = #{chatId}")
    int getByChatIdAndUserId(Integer chatId, Integer userId);

    // 用户关注
    @Insert("insert into tb_chat_like (chat_id, user_id, created_time) VALUES (#{chatId},#{userId},#{now})")
    void save(Integer chatId, Integer userId, LocalDateTime now);

    // 用户取消点赞
    @Delete("delete from tb_chat_like where user_id = #{userId} and chat_id = #{chatId}")
    void delete(Integer chatId, Integer userId);

    // 删除帖子点赞数据
    @Delete("delete from tb_chat_like where chat_id = #{chatId}")
    void deleteByChatId(Integer chatId);
}
