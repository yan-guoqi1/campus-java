package com.chat.service;

public interface RankingRedis {

    void collectionChat(Integer chatId);

    // 点赞帖子 3分
    void likeChat(Integer chatId);

    // 回复帖子 5分
    void commentChat(Integer chatId);

    void clCollectionChat(Integer chatId);

    // 点赞帖子 3分
    void clLikeChat(Integer chatId);

    // 回复帖子 5分
    void clCommentChat(Integer chatId,Integer number);
}
