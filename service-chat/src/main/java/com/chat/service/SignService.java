package com.chat.service;

import DO.chat.SignDO;
import VO.SignVO;

import java.util.List;

public interface SignService {


    // 获取标签
    List<SignVO> getSign();

    // 判断数组中标签是否存在
    void judgeSignIsExist(Integer signId);
}
