package com.chat.service;

import DTO.*;
import VO.ChatVO;
import VO.Feight.UserMsgByChatVO;
import VO.RankingVO;
import VO.ReportSignVO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ChatService {
    // 帖子文件的保存
    void publishChat(String userId, PublishChatDTO publishChatDTO, MultipartFile[] files);

    // 获取该用户的发帖数量
    UserMsgByChatVO getChatNumberByUserId(Integer userId);

    // 获取帖子id
    List<Integer> getChatIdArray(ChatPageDTO chatPageDTO);

    // 根据id获取帖子具体信息
    ChatVO getChatById(Integer chatId, Integer userId);

    // 用户收藏
    void collectionById(String userId, Integer chatId);

    // 用户关注
    void like(Integer userId, Integer chatId);

    // 用户取消收藏
    void deleteCollectionById(Integer userId, Integer chatId);

    // 用户取消关注
    void deleteLike(Integer integer, Integer chatId);

    // 用户评论
    void pubChatComment(PubCommentDTO pubCommentDTO, Integer userId);

    // 获取用户的发帖id
    List<Integer> getUserPubChatId(ChatPageByUserPubDTO chatPageByUserPubDTO);

    // 获取用户的收藏id
    List<Integer> getUserChatIdByCollection(ChatPageByCollectionDTO chatPageByCollectionDTO);

    // 用户删除帖子
    void deleteChat(Integer integer, Integer chatId);

    // 用户举报帖子
    void reportChat(ReportChatDTO reportChatDTO, String userId, MultipartFile[] files);

    // 获取举报标签
    ReportSignVO getReportSign();


    void MQdeleteCollection(Integer chatId);

    void MQdeleteLike(Integer chatId);

    void MqdeletePicture(Integer chatId);

    List<RankingVO> getRanking(RankingChatDTO rankingChatDTO);

    void MQupdateComment(Integer chatId, Integer number);
}
