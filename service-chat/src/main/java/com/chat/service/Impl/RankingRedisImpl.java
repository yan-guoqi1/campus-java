package com.chat.service.Impl;


import DO.chat.ChatDO;
import com.chat.mapper.ChatMapper;
import com.chat.service.RankingRedis;
import constant.RedisPreKeyConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Service;

import static com.utils.RankingUtils.getSeconds;

/**
 * 帖子的排行榜redis实现类，
 */
@Service
@Slf4j
public class RankingRedisImpl implements RankingRedis {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    private ChatMapper chatMapper;

    // 收藏帖子 1分
    public void collectionChat(Integer chatId){
        ChatDO chatDO = chatMapper.getById(chatId);
        if (chatDO == null){
            return;
        }
        Long seconds = getSeconds(chatDO.getCreatedTime());

        // 创建ZSetOperations对象
        ZSetOperations<String, Object> zSetOperations = redisTemplate.opsForZSet();

        // 获取成员的当前分数
        String value = seconds + "_" + chatId;

        Double scoreDay = zSetOperations.score(RedisPreKeyConstant.RANKING_DAY, value);
        if (scoreDay!=null){
            zSetOperations.add(RedisPreKeyConstant.RANKING_DAY,value,scoreDay+1);
        }


        Double scoreWeek = zSetOperations.score(RedisPreKeyConstant.RANKING_WEEK, value);
        if (scoreWeek!=null){
            zSetOperations.add(RedisPreKeyConstant.RANKING_WEEK,value,scoreWeek+1);
        }

        Double scoreMonth = zSetOperations.score(RedisPreKeyConstant.RANKING_MONTH, value);
        if (scoreMonth!=null){
            zSetOperations.add(RedisPreKeyConstant.RANKING_MONTH,value,scoreMonth+1);
        }
    }

    // 点赞帖子 3分
    public void likeChat(Integer chatId){
        ChatDO chatDO = chatMapper.getById(chatId);
        if (chatDO == null){
            return;
        }
        Long seconds = getSeconds(chatDO.getCreatedTime());

        // 创建ZSetOperations对象
        ZSetOperations<String, Object> zSetOperations = redisTemplate.opsForZSet();

        // 获取成员的当前分数
        String value = seconds + "_" + chatId;

        Double scoreDay = zSetOperations.score(RedisPreKeyConstant.RANKING_DAY, value);
        if (scoreDay!=null){
            zSetOperations.add(RedisPreKeyConstant.RANKING_DAY,value,scoreDay+3);
        }


        Double scoreWeek = zSetOperations.score(RedisPreKeyConstant.RANKING_WEEK, value);
        if (scoreWeek!=null){
            zSetOperations.add(RedisPreKeyConstant.RANKING_WEEK,value,scoreWeek+3);
        }

        Double scoreMonth = zSetOperations.score(RedisPreKeyConstant.RANKING_MONTH, value);
        if (scoreMonth!=null){
            zSetOperations.add(RedisPreKeyConstant.RANKING_MONTH,value,scoreMonth+3);
        }
    }

    // 回复帖子 5分
    public void commentChat(Integer chatId){
        ChatDO chatDO = chatMapper.getById(chatId);
        if (chatDO == null){
            return;
        }
        Long seconds = getSeconds(chatDO.getCreatedTime());

        // 创建ZSetOperations对象
        ZSetOperations<String, Object> zSetOperations = redisTemplate.opsForZSet();

        // 获取成员的当前分数
        String value = seconds + "_" + chatId;

        Double scoreDay = zSetOperations.score(RedisPreKeyConstant.RANKING_DAY, value);
        if (scoreDay!=null){
            zSetOperations.add(RedisPreKeyConstant.RANKING_DAY,value,scoreDay+5);
        }


        Double scoreWeek = zSetOperations.score(RedisPreKeyConstant.RANKING_WEEK, value);
        if (scoreWeek!=null){
            zSetOperations.add(RedisPreKeyConstant.RANKING_WEEK,value,scoreWeek+5);
        }

        Double scoreMonth = zSetOperations.score(RedisPreKeyConstant.RANKING_MONTH, value);
        if (scoreMonth!=null){
            zSetOperations.add(RedisPreKeyConstant.RANKING_MONTH,value,scoreMonth+5);
        }
    }

    // 取消收藏帖子 1分
    public void clCollectionChat(Integer chatId) {
        ChatDO chatDO = chatMapper.getById(chatId);
        if (chatDO == null){
            return;
        }
        Long seconds = getSeconds(chatDO.getCreatedTime());

        // 创建ZSetOperations对象
        ZSetOperations<String, Object> zSetOperations = redisTemplate.opsForZSet();

        // 获取成员的当前分数
        String value = seconds + "_" + chatId;

        Double scoreDay = zSetOperations.score(RedisPreKeyConstant.RANKING_DAY, value);
        if (scoreDay!=null){
            zSetOperations.add(RedisPreKeyConstant.RANKING_DAY,value,scoreDay-1);
        }


        Double scoreWeek = zSetOperations.score(RedisPreKeyConstant.RANKING_WEEK, value);
        if (scoreWeek!=null){
            zSetOperations.add(RedisPreKeyConstant.RANKING_WEEK,value,scoreWeek-1);
        }

        Double scoreMonth = zSetOperations.score(RedisPreKeyConstant.RANKING_MONTH, value);
        if (scoreMonth!=null){
            zSetOperations.add(RedisPreKeyConstant.RANKING_MONTH,value,scoreMonth-1);
        }
    }

    // 取消点赞帖子 3分
    public void clLikeChat(Integer chatId) {
        ChatDO chatDO = chatMapper.getById(chatId);
        if (chatDO == null){
            return;
        }
        Long seconds = getSeconds(chatDO.getCreatedTime());

        // 创建ZSetOperations对象
        ZSetOperations<String, Object> zSetOperations = redisTemplate.opsForZSet();

        // 获取成员的当前分数
        String value = seconds + "_" + chatId;

        Double scoreDay = zSetOperations.score(RedisPreKeyConstant.RANKING_DAY, value);
        if (scoreDay!=null){
            zSetOperations.add(RedisPreKeyConstant.RANKING_DAY,value,scoreDay-3);
        }


        Double scoreWeek = zSetOperations.score(RedisPreKeyConstant.RANKING_WEEK, value);
        if (scoreWeek!=null){
            zSetOperations.add(RedisPreKeyConstant.RANKING_WEEK,value,scoreWeek-3);
        }

        Double scoreMonth = zSetOperations.score(RedisPreKeyConstant.RANKING_MONTH, value);
        if (scoreMonth!=null){
            zSetOperations.add(RedisPreKeyConstant.RANKING_MONTH,value,scoreMonth-3);
        }
    }

    // 取消评论帖子 5分
    public void clCommentChat(Integer chatId,Integer number) {
        ChatDO chatDO = chatMapper.getById(chatId);
        if (chatDO == null){
            return;
        }
        Long seconds = getSeconds(chatDO.getCreatedTime());

        // 创建ZSetOperations对象
        ZSetOperations<String, Object> zSetOperations = redisTemplate.opsForZSet();

        // 获取成员的当前分数
        String value = seconds + "_" + chatId;

        Double scoreDay = zSetOperations.score(RedisPreKeyConstant.RANKING_DAY, value);
        if (scoreDay!=null){
            zSetOperations.add(RedisPreKeyConstant.RANKING_DAY,value,scoreDay-5*number);
        }


        Double scoreWeek = zSetOperations.score(RedisPreKeyConstant.RANKING_WEEK, value);
        if (scoreWeek!=null){
            zSetOperations.add(RedisPreKeyConstant.RANKING_WEEK,value,scoreWeek-5*number);
        }

        Double scoreMonth = zSetOperations.score(RedisPreKeyConstant.RANKING_MONTH, value);
        if (scoreMonth!=null){
            zSetOperations.add(RedisPreKeyConstant.RANKING_MONTH,value,scoreMonth-5*number);
        }
    }
}
