package com.chat.service.Impl;

import VO.SignVO;
import com.chat.mapper.SignMapper;
import com.chat.service.SignService;
import Exception.BaseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class SignServiceImpl implements SignService {

    @Autowired
    private SignMapper signMapper;

    // 获取帖子标签
    public List<SignVO> getSign() {
        SignVO signVO = new SignVO();
        signVO.setSign("热门");
        signVO.setId(0);
        List<SignVO> sign = signMapper.getSign();
        sign.add(0,signVO);
        return sign;
    }

    // 判断标签是否存在
    public void judgeSignIsExist(Integer signId) {
        Integer[] ids = signMapper.getSignId();

        for (Integer id : ids) {
            if (Objects.equals(id, signId)){
                return;
            }
        }

        throw new BaseException();
    }
}
