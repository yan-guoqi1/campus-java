package com.feign.clients;


import VO.Feight.ChatByUserMsgVO;
import VO.Feight.CommentUserMsgVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import result.Result;

@FeignClient(value = "user-service")
public interface UserClient {

    /**
     * 查询该人是否存在
     * @param id 用户id
     * @return true 存在 false 不存在
     */
    @GetMapping("/feign/user/{id}")
    boolean findById(@PathVariable("id") Integer id);


    /**
     * 获取用户的基本信息
     * @param chatUserId
     * @param userId
     * @return
     * 201 表示用户不存在
     */
    @GetMapping("/feign/user/getChatUserMsg")
    Result<ChatByUserMsgVO> getChatUserMsg(@RequestParam("chatUserId") Integer chatUserId,
                                           @RequestParam("userId")Integer userId);

    /**
     * 获取用户评论时的头像以及昵称信息
     * @param userId 用户id
     * @return
     */
    @GetMapping("/feign/user//getCommentUserMsg/{userId}")
    Result<CommentUserMsgVO> getCommentUserMsg(@PathVariable("userId") Integer userId);
}
