package com.feign.clients;


import VO.Feight.UserMsgByChatVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "chat-service")
public interface ChatClient {

    /**
     * 查询该用户的发帖数量
     * @param userId 用户id
     * @return 数量
     */
    @GetMapping("/feign/chat/getChatNumberByUserId/{userId}")
    UserMsgByChatVO getChatNumberByUserId(@PathVariable("userId") Integer userId);
}
