package com.feign.clients;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "comment-service")
public interface commentClient {

    @GetMapping("/comment/test/{id}")
    String test(@PathVariable("id") Integer id);
}
