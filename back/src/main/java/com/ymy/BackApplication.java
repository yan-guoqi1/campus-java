package com.ymy;

import com.feign.clients.ChatClient;
import com.feign.clients.commentClient;
import com.feign.config.DefaultFeignConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients(clients = {ChatClient.class, commentClient.class},defaultConfiguration = DefaultFeignConfiguration.class)
public class BackApplication {
    public static void main(String[] args) {
        SpringApplication.run(BackApplication.class);
    }
}